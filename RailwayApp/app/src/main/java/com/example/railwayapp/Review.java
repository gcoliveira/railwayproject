
package com.example.railwayapp;


public class Review {
    private float rating;
    private String description;

    public Review(float rating, String description) {
        this.rating = rating;
        this.description = description;
    }


    public Review(){
        this.rating = 0;
        this.description = "";
    }

    @Override
    public String toString() {
        return "Review{" +
            ", destination=" + rating +
            ", travelDate=" + description +
            '}';
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRating() {
        return rating;
    }

    public String getDescription() {
        return description;
    }

}

