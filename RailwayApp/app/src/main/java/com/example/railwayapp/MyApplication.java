package com.example.railwayapp;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.engineio.client.Transport;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Manager;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MyApplication extends Application {

    private KeyManager keyManager = new KeyManager();
    private Socket ioSocket;
    private User currentUser;
    private List<Card> creditCards = new ArrayList<>();
    private List<Stop> stops = new ArrayList<>();
    private List<Trip> trips = new ArrayList<>();
    private List<Station> stations = new ArrayList<>();
    private boolean loggedin = false;

    {
        try {
            ioSocket = IO.socket("http://192.168.1.17:3000");
        } catch (URISyntaxException e) {
            Log.d("myTag", e.getMessage());
        }
    }
    public void connect(){

        try {
            ioSocket.connect();
        }catch(Exception e){
            Log.d("myTag", e.getMessage());

        }
    }

    public Socket getSocket(){
        return ioSocket;
    }

    public void setUser(User user){
        this.currentUser = user;
    }
    public User getUser(){
        return this.currentUser;
    }


    public Stop getStopById(int stopId){
        for(Stop s : this.stops){
            if(s.getStopId() == stopId)
                return s;
        }
        return null;
    }

    public void addCreditCard(Card creditCard){
        this.creditCards.add(creditCard);
    }

    public List<Card> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(List<Card> creditCard) {
        this.creditCards = creditCard;
    }

    public void removeCreditCard(Card creditCard){
        this.creditCards.remove(creditCard);
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public List<Stop> getStopsInStation (int stationNumber){

        List<Stop> tempStops = new ArrayList<>();

        for (Stop s: this.stops) {
            if(s.getStationNumber() == stationNumber){
                tempStops.add(s);
            }
        }

        return tempStops;
    }

    public List<Stop> getStopsInStationMovingUp (int stationNumber){

        List<Stop> tempStops = new ArrayList<>();

        for (Stop s:getStopsInStation(stationNumber)) {
            if(s.getNextStationNumber() > stationNumber){
                tempStops.add(s);
            }
        }

        return tempStops;
    }

    public List<Stop> getStopsInStationMovingDown (int stationNumber){

        List<Stop> tempStops = new ArrayList<>();

        for (Stop s:getStopsInStation(stationNumber)) {
            if(s.getNextStationNumber() < stationNumber && s.getNextStationNumber()!=0){
                tempStops.add(s);
            }
        }

        return tempStops;
    }

    public Trip getTripWithId(int id){
        for(Trip t : trips){
            if(t.getTripId() == id)
                return t;
        }
        return null;
    }

    public void addStop(Stop stop){
        this.stops.add(stop);
    }

    public void addStation(Station station){
        this.stations.add(station);
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public List<Station> getStations() {
        return stations;
    }

    public Station getStationByName(String stationName){

        for (Station s: this.stations) {
            if(s.getName().equalsIgnoreCase(stationName)){
                return s;
            }
        }
        return null;
    }

    public Station getStationByNumber(int stationNumber){

        for (Station s: this.stations) {
            if(stationNumber == s.getStationNumber()){
                return s;
            }
        }
        return null;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void addTrip(Trip trip){
        this.trips.add(trip);
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public boolean isLoggedin() {
        return loggedin;
    }

    public void setLoggedin(boolean loggedin) {
        this.loggedin = loggedin;
    }

    public void clearData() {
        this.creditCards = new ArrayList<>();
        this.stops = new ArrayList<>();
        this.trips = new ArrayList<>();
        this.stations = new ArrayList<>();
    }
}