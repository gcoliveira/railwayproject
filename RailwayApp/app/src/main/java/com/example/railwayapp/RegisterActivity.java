package com.example.railwayapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;


public class RegisterActivity  extends AppCompatActivity {
    Spinner countriesSpinner;
    EditText nameEditText, emailEditText, passwordEditText, cardNumberEditText, cardYearEditText, cardMonthEditText;
    LinearLayout cardLL;
    DatePicker birthdayDatePicker;
    RelativeLayout loadingPanelRegisterActivity;
    Button registerButton;
    Switch cardSwitch;
    Socket ioSocket;
    RadioGroup cardTypeRadioGroup;
    RadioButton maestroRadioButton, visaRadioButton, masterCardRadioButton;

    SharedPreferences sharedPref;
    MyApplication myApp;

    String name, email, password, date, nationality;
    String cardNumber, cardType, cardValidity;


    private final String KEY_ALIAS = "RailwayKey_";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //this.pubkey = getIntent().getStringExtra("publicKeyRSA");

        myApp = (MyApplication) getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();

        sharedPref = getApplicationContext().getSharedPreferences("railwaySP", Context.MODE_PRIVATE);

        getSupportActionBar().setTitle("Register");

        countriesSpinner = findViewById(R.id.NationalitySpinner);
        nameEditText = findViewById(R.id.nameEditText);
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        birthdayDatePicker = findViewById(R.id.birthDayDatePicker);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -10);
        birthdayDatePicker.setMaxDate(calendar.getTimeInMillis());

        cardLL = findViewById(R.id.cardLinearLayout);
        cardNumberEditText = findViewById(R.id.cardNumberEditBox);
        cardMonthEditText = findViewById(R.id.cardMonthEditText);
        cardYearEditText = findViewById(R.id.cardYearEditText);
        cardSwitch = findViewById(R.id.cardSwitch);
        cardTypeRadioGroup = findViewById(R.id.cardTypeRadioGroup);

        registerButton = findViewById(R.id.registerButton);
        masterCardRadioButton = findViewById(R.id.masterCardRadioButton);
        visaRadioButton = findViewById(R.id.visaRadioButton);
        maestroRadioButton = findViewById(R.id.maestroRadioButton);

        loadingPanelRegisterActivity = findViewById(R.id.loadingPanelRegisterActivity);


        List<String> countries = new ArrayList<>();
        String[] isoCountries = Locale.getISOCountries();
        for (String country : isoCountries) {
            Locale locale = new Locale("en", country);
            countries.add(locale.getDisplayCountry());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, countries);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        countriesSpinner.setAdapter(adapter);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ioSocket.connected()){
                    Toast.makeText(getApplicationContext(), "Not connected to Server!", Toast.LENGTH_LONG);
                    return;
                }
                name = nameEditText.getText().toString();
                email = emailEditText.getText().toString();
                password = passwordEditText.getText().toString();
                nationality = countriesSpinner.getSelectedItem().toString();
                date = birthdayDatePicker.getDayOfMonth() + "/" + (birthdayDatePicker.getMonth()+1) + '/' + birthdayDatePicker.getYear();

                if(!checkFields()){
                    return;
                }

                //HIDE THE KEYBOARD AFTER PRESSING THE BUTTON
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }


                myApp.getKeyManager().GenerateKeyPair(getApplicationContext(), name);
                String pubKeyAsPEM = myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getPubKey());
                if(pubKeyAsPEM == null){
                    Log.e("PubKey Error", "Pub key return as null");
                    return;
                }

                if (cardSwitch.isChecked()) {
                    cardNumber = cardNumberEditText.getText().toString();
                    if (cardTypeRadioGroup.getCheckedRadioButtonId() == -1)
                    {
                        maestroRadioButton.setError("Select a Card Type");
                        return;
                    }
                    RadioButton auxRB = findViewById(cardTypeRadioGroup.getCheckedRadioButtonId());//cardTypeEditText.getText().toString();
                    cardType = auxRB.getText().toString();

                    String valMonth = null, valYear = null;

                    valMonth = cardMonthEditText.getText().toString();
                    valYear = cardYearEditText.getText().toString();

                    if(valMonth == null || valMonth.isEmpty() || Integer.parseInt(valMonth) > 12 || Integer.parseInt(valMonth) <= 0){
                        cardMonthEditText.setError("Invalid");
                        return;
                    }
                    int lastTwoDigits = Calendar.getInstance().get(Calendar.YEAR) % 100;
                    if(valYear == null || valYear.isEmpty() || Integer.parseInt(valYear) < lastTwoDigits){
                        cardYearEditText.setError("Invalid");
                        return;
                    }else if(Integer.parseInt(valYear) == lastTwoDigits &&
                            Integer.parseInt(valMonth) <= Calendar.getInstance().get(Calendar.MONTH)){
                        cardMonthEditText.setError("Invalid");
                        return;
                    }

                    cardValidity =  valMonth + "/" + valYear;


                    if (cardNumber == null || cardNumber.isEmpty() || cardNumber.length() < 10) {
                        cardNumberEditText.setError("Card number not valid");
                        return;
                    }

                    if (cardType == null || cardType.isEmpty()) {
                        //cardTypeEditText.setError("Card type not valid");
                        return;
                    }
                    if (cardValidity == null || cardValidity.isEmpty()) {
                        cardMonthEditText.setError("Card Validity not valid");
                        cardYearEditText.setError("Card Validity not valid");
                        return;
                    }
                    //GenerateKeyPair(name);
                    loadingPanelRegisterActivity.setVisibility(View.VISIBLE);
                    ioSocket.emit("registerWithCard", name, email, password, nationality, date, pubKeyAsPEM , cardNumber, cardType, cardValidity);
                } else {
                    //GenerateKeyPair(name);
                    loadingPanelRegisterActivity.setVisibility(View.VISIBLE);
                    ioSocket.emit("registerWithOutCard", name, email, password, nationality, date, pubKeyAsPEM);
                }
            }
        });
    }

    private void setListening() {

        ioSocket.off("nameNotAvailable");
        ioSocket.off("cardNotAvailable");
        ioSocket.off("emailNotAvailable");
        ioSocket.off("RegisterSuccess");

        ioSocket.on("nameNotAvailable", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("GOT SOME ARGUMENTS IN NAMENOTAVAILABLE:" + args[0] + "; ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        nameEditText.setError("Name not available");
                        loadingPanelRegisterActivity.setVisibility(View.GONE);
                    }
                });
            }
        });

        ioSocket.on("cardNotAvailable", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("GOT SOME ARGUMENTS IN CARDNOTAVAILABLE:" + args[0] + "; ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cardNumberEditText.setError("Card not available");
                        loadingPanelRegisterActivity.setVisibility(View.GONE);
                    }
                });
            }
        });

        ioSocket.on("emailNotAvailable", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("GOT SOME ARGUMENTS IN CARDNOTAVAILABLE:" + args[0] + "; ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        emailEditText.setError("Email not available");
                        loadingPanelRegisterActivity.setVisibility(View.GONE);
                    }
                });
            }
        });

        ioSocket.on("RegisterSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("REGISTER WAS A SUCCESS");
                MyApplication myApp = (MyApplication) getApplicationContext();
                myApp.setUser(new User((String)args[0], name, email, password, nationality, date));

                if(cardSwitch.isChecked())
                    myApp.addCreditCard(new Card(cardType, cardValidity, cardNumber));

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("uuid", (String)args[0]);
                editor.commit();

                Log.println(Log.ASSERT, "saving uuid to sp", sharedPref.getString("uuid", null));

                myApp.getKeyManager().setServerPubKey(myApp.getKeyManager().GeneratePublicKeyFromPEM((String)args[1]));

                Log.println(Log.ASSERT, "Set server Pub key:", myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getServerPubKey()));

                Intent registerData = new Intent();
                setResult(RESULT_OK, registerData);
                finish();
            }
        });
    }



    public void switchCardSwitch(View view) {
        if(!cardSwitch.isChecked()){
            cardTypeRadioGroup.setEnabled(false);
            maestroRadioButton.setEnabled(false);
            visaRadioButton.setEnabled(false);
            masterCardRadioButton.setEnabled(false);

            cardMonthEditText.setEnabled(false);
            cardYearEditText.setEnabled(false);

            cardNumberEditText.setEnabled(false);
        }else{
            cardTypeRadioGroup.setEnabled(true);
            maestroRadioButton.setEnabled(true);
            visaRadioButton.setEnabled(true);
            masterCardRadioButton.setEnabled(true);

            cardMonthEditText.setEnabled(true);
            cardYearEditText.setEnabled(true);

            cardNumberEditText.setEnabled(true);

        }
    }

    private boolean checkFields(){

        System.out.println("THINGS: "+name+"; "+email+"; "+password);
        if (name == null || name.isEmpty() || name.length() < 7 || name.length() > 18) {
            nameEditText.setError("Name not valid");
            return false;
        }
        if (email == null || email.isEmpty() || android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEditText.setError("Email not valid");
            return false;
        }
        if (password == null || password.isEmpty() || password.length() < 6 || password.length() > 18) {
            passwordEditText.setError("Password not valid");
            return false;
        }
        return true;
    }

}
