package com.example.railwayapp;

import java.util.ArrayList;
import java.util.List;

public class Trip {

    private int tripId;
    private int initialStationNumber;
    private int finalStationNumber;
    private String initialTime;
    private String finalTime;
    private List<Stop> stops = new ArrayList<>();

    public Trip(int tripId, int initialStationNumber, int finalStationNumber, String initialTime, String finalTime) {
        this.tripId = tripId;
        this.initialStationNumber = initialStationNumber;
        this.finalStationNumber = finalStationNumber;
        this.initialTime = initialTime;
        this.finalTime = finalTime;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public int getInitialStationNumber() {
        return initialStationNumber;
    }

    public void setInitialStationNumber(int initialStationNumber) {
        this.initialStationNumber = initialStationNumber;
    }

    public int getFinalStationNumber() {
        return finalStationNumber;
    }

    public void setFinalStationNumber(int finalStationNumber) {
        this.finalStationNumber = finalStationNumber;
    }

    public String getInitialTime() {
        return initialTime;
    }

    public void setInitialTime(String initialTime) {
        this.initialTime = initialTime;
    }

    public String getFinalTime() {
        return finalTime;
    }

    public void setFinalTime(String finalTime) {
        this.finalTime = finalTime;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public void addStop(Stop stop){
        this.stops.add(stop);
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public Stop getStopWithId(int id){
        for(Stop s: stops){
            if(s.getStopId() == id)
                return s;
        }
        return null;
    }

    public Stop getStopWithStationNumber(int id){
        for(Stop s: stops){
            if(s.getStationNumber() == id)
                return s;
        }
        return null;
    }

    public String toString(){
        String s = "ID:"+tripId+"; "+initialStationNumber+"["+initialTime+"] -> "+finalStationNumber+"["+finalTime+"];\nStops: ";
        for(Stop stop : stops)
            s += stop.getStopId() + " ";

        return s;
    }
}
