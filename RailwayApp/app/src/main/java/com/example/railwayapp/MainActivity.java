package com.example.railwayapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    private Socket ioSocket;
    private MyApplication myApp;


    public FragmentRefreshListener getFragmentRefreshListener() {return fragmentRefreshListener;}
    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {this.fragmentRefreshListener = fragmentRefreshListener; }
    private FragmentRefreshListener fragmentRefreshListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.action_travel, R.id.action_timetable, R.id.action_history, R.id.action_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);



        myApp = (MyApplication) getApplicationContext();
        myApp.connect();
        ioSocket = myApp.getSocket();
        setListening();

        myApp.getKeyManager().checkKeysInAndroidKeyStore();

        //myApp.getKeyManager().deleteKeyStoreEntries();
        if(!myApp.isLoggedin())
            ShowLoginPage();
        //getFragmentRefreshListener().onRefresh();


    }

    private void ShowLoginPage() {
        Intent loginPage = new Intent(this, LoginActivity.class);
        startActivityForResult(loginPage, 1);
    }


    private void setHomePage() {
        //print the views with the user values
        if(getFragmentRefreshListener()!=null){
            getFragmentRefreshListener().onRefresh();
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                System.out.println("GOT BACK FROM LOGIN SUCCESS");
            }
        }
    }

    private void setListening() {

        ioSocket.off("getInitialDataFromDB");
        ioSocket.off("loggedIn");

        ioSocket.on("getInitialDataFromDB", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    myApp.clearData();
                    JSONArray stations =  new JSONArray(args[0].toString());
                    for(int i=0; i < stations.length(); i++){
                        JSONObject tempStation = stations.getJSONObject(i);
                        myApp.addStation(new Station(tempStation.getInt("stationNumber"), tempStation.getString("name")));
                        //Log.println(Log.ASSERT, "station received:", "Name: "+tempStation.getString("name")+ "; Number: "+tempStation.getInt("stationNumber"));
                    }

                    JSONArray stops =  new JSONArray(args[1].toString());
                    for(int i=0; i < stops.length(); i++){
                        JSONObject tempStop = stops.getJSONObject(i);
                        String test = tempStop.getString("nextStationNumber");
                        if(test == "null") {
                            myApp.addStop(new Stop(tempStop.getInt("stopId"), tempStop.getString("stopTime"),
                                    tempStop.getInt("stationNumber"), 0, tempStop.getInt("seatsToNextStop"), tempStop.getInt("tripId")));
                        }else{
                            myApp.addStop(new Stop(tempStop.getInt("stopId"), tempStop.getString("stopTime"),
                                    tempStop.getInt("stationNumber"), tempStop.getInt("nextStationNumber"), tempStop.getInt("seatsToNextStop"), tempStop.getInt("tripId")));
                        }
                    }

                    //for(Stop s:myApp.getStops())
                        //Log.println(Log.ASSERT, "trips:", s.toString());

                    JSONArray trips =  new JSONArray(args[2].toString());
                    for(int i=0; i < trips.length(); i++){
                        JSONObject tempTrip = trips.getJSONObject(i);
                        Trip aux = new Trip(tempTrip.getInt("tripId"), tempTrip.getInt("initialStationNumber"), tempTrip.getInt("finalStationNumber"),
                                tempTrip.getString("initialTime"), tempTrip.getString("finalTime"));

                        for(Stop s: myApp.getStops()){
                            if(s.getTripId() == tempTrip.getInt("tripId")) {
                                aux.addStop(s);
                            }
                        }
                        myApp.addTrip(aux);

                    }

                    if(args[3] != null){
                        JSONArray creditCards =  new JSONArray(args[3].toString());
                        for(int i=0; i < creditCards.length(); i++){
                            JSONObject tempCard = creditCards.getJSONObject(i);
                            Log.println(Log.ASSERT, "Cards:", tempCard.toString());
                            Card auxCard = new Card(tempCard.getString("type"), tempCard.getString("validity"), tempCard.getString("number"));
                            myApp.addCreditCard(auxCard);
                        }
                    }else{
                        Log.println(Log.ASSERT, "Cards:", "CAME BACK NULL");
                    }

                    //for(Trip t:myApp.getTrips())
                        //Log.println(Log.ASSERT, "trips:", t.toString());
                    setHomePage();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public interface FragmentRefreshListener{
        void onRefresh();
    }

}