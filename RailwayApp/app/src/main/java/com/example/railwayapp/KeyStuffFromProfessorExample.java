package com.example.railwayapp;

import android.content.Intent;
import android.os.Bundle;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

public class KeyStuffFromProfessorExample extends AppCompatActivity {
    final String TAG = "GenTicket";
    UUID TicUUID, UsUUID;
    boolean hasKey = false;
    PrivateKey pri;
    PublicKey pub;
    byte[] signature, encTicket;

    EditText edTID, edUID, edTrain, edOri, edDest, edDay, edMon, edYear;
    TextView tvNoKey, tvKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keystuff);
        edTID = findViewById(R.id.ed_uuid);
        edUID = findViewById(R.id.ed_usid);
        edTrain = findViewById((R.id.ed_train));
        edOri = findViewById(R.id.ed_origin);
        edDest = findViewById((R.id.ed_dest));
        edDay = findViewById(R.id.ed_day);
        edMon = findViewById(R.id.ed_month);
        edYear = findViewById(R.id.ed_year);
        tvNoKey = findViewById(R.id.tv_nokey);
        tvKey = findViewById(R.id.tv_pubkey);
        findViewById(R.id.bt_ugen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {      // Tid generate button
                TicUUID = UUID.randomUUID();
                edTID.setText(TicUUID.toString());
            }
        });
        findViewById(R.id.bt_usgen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {    // Uid generate button
                UsUUID = UUID.randomUUID();
                edUID.setText(UsUUID.toString());
            }
        });
       /* findViewById(R.id.bt_gentic).setOnClickListener((v)->genTic());    // buttons
        findViewById(R.id.bt_dectic).setOnClickListener((v)->decTic());
        findViewById(R.id.bt_genkey).setOnClickListener((v)->genKeys());
        findViewById(R.id.bt_send).setOnClickListener((v)->sendKey());
        findViewById(R.id.bt_showkey).setOnClickListener((v)->showKey());*/

        // See if we have already the keys and read them
        try {
            KeyStore ks = KeyStore.getInstance(Constants.ANDROID_KEYSTORE);
            ks.load(null);
            KeyStore.Entry entry = ks.getEntry(Constants.keyname, null);
            hasKey = (entry != null);
            if (hasKey) {
                pub = ((KeyStore.PrivateKeyEntry)entry).getCertificate().getPublicKey();
                pri = ((KeyStore.PrivateKeyEntry)entry).getPrivateKey();
                tvNoKey.setText("");
            }
        }
        catch (Exception e) {
            hasKey = false;
        }

        findViewById(R.id.bt_send).setEnabled(hasKey);
        findViewById(R.id.bt_showkey).setEnabled(hasKey);
        findViewById(R.id.bt_genkey).setEnabled(!hasKey);
    }

    //
    //  Generate a ticket (encrypted and signed)
    //
    void genTic() {
        ByteBuffer ticket;    // useful class to accumulate bytes

        if (!hasKey || edTID.getText().toString().length() == 0 || edUID.getText().toString().length() == 0) {
            tvNoKey.setText(R.string.msg_empty);
            return;
        }

        // Build the ticket in binary (into a ByteBuffer object)
        int len = 16 + 16 + 6;  // 2 ID's + 6 bytes

        ticket = ByteBuffer.allocate(len);
        ticket.putLong(TicUUID.getMostSignificantBits());
        ticket.putLong(TicUUID.getLeastSignificantBits());              // Ticket ID (16 bytes)
        ticket.putLong(UsUUID.getMostSignificantBits());
        ticket.putLong(UsUUID.getLeastSignificantBits());               // User ID (16 bytes)
        ticket.put(Byte.parseByte(edTrain.getText().toString()));       // Train nr (1..6)
        ticket.put(Byte.parseByte(edOri.getText().toString()));         // Origin Station nr (1..5)
        ticket.put(Byte.parseByte(edDest.getText().toString()));        // Destination Station nr (1..5)
        ticket.put(Byte.parseByte(edDay.getText().toString()));         // Day (1..31)
        ticket.put(Byte.parseByte(edMon.getText().toString()));         // Month (1..12)
        ticket.put(Byte.parseByte(edYear.getText().toString()));        // Year nr (0..99)

        // sign the ticket (plain text) using a private key (it should be the train company key)
        // But, just to illustrate the method we use the user private key
        try {
            Signature sg = Signature.getInstance(Constants.SIGN_ALGO);
            sg.initSign(pri);
            sg.update(ticket.array());
            signature = sg.sign();
        }
        catch (Exception e) {
            tvNoKey.setText(e.toString());
            return;
        }

        // encrypt the ticket with the private key (obtain a 64 byte result)
        try {
            Cipher cipher = Cipher.getInstance(Constants.ENC_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, pri);
            encTicket = cipher.doFinal(ticket.array());
            // display encryptd ticket + signature
            tvKey.setText(getResources().getString(R.string.msg_enctag, encTicket.length, byteArrayToHex(encTicket),
                    signature.length, byteArrayToHex(signature)));
            tvNoKey.setText("");
        }
        catch (Exception e) {
            tvNoKey.setText(e.toString());
            return;
        }

        // send E(pri, ticket) || S(pri, ticket) to be shown as a QR-code
       /* Intent qrAct = new Intent(this, QRActivity.class);
        byte[] all_ticket = new byte[encTicket.length + signature.length];
        System.arraycopy(encTicket, 0, all_ticket, 0, encTicket.length);
        System.arraycopy(signature, 0, all_ticket, encTicket.length, signature.length);
        qrAct.putExtra("data", all_ticket);
        startActivity(qrAct);*/
    }

    //
    //  decode the ticket as if it were received from a user (encrypted ticket + signature)
    //
    void decTic() {
        byte[] clearTicket;
        boolean verified;

        if (encTicket == null || encTicket.length == 0) {
            tvNoKey.setText(R.string.msg_notag);
            return;
        }

        // clearTicket = D(pub, encTicket)
        try {
            Cipher cipher = Cipher.getInstance(Constants.ENC_ALGO);
            cipher.init(Cipher.DECRYPT_MODE, pub);
            clearTicket = cipher.doFinal(encTicket);
        }
        catch (Exception e) {
            tvNoKey.setText(e.getMessage());
            return;
        }

        ByteBuffer bTicket = ByteBuffer.wrap(clearTicket);
        UUID Tid = new UUID(bTicket.getLong(), bTicket.getLong());
        UUID Uid = new UUID(bTicket.getLong(), bTicket.getLong());
        byte train = bTicket.get();                                      // Train nr (1..6)
        byte ori = bTicket.get();                                        // Origin Station nr (1..5)
        byte dest = bTicket.get();                                       // Destination Station nr (1..5)
        byte day = bTicket.get();                                        // Train nr (1..6)
        byte month = bTicket.get();                                      // Origin Station nr (1..5)
        byte year = bTicket.get();                                       // Destination Station nr (1..5)

        // verify the signature using the correspondent public key and the plaintext ticket
        try {
            Signature sg = Signature.getInstance(Constants.SIGN_ALGO);
            sg.initVerify(pub);
            sg.update(bTicket.array());
            verified = sg.verify(signature);
        }
        catch (Exception ex) {
            tvNoKey.setText(ex.getMessage());
            return;
        }

        // show the ticket in text
        String text = "Verified: " + verified + "\n" +
                "DecTic (" + clearTicket.length + "):\n" + byteArrayToHex(clearTicket) + "\n\n" +
                "TID: " + Tid.toString() + "\n" +
                "UID: " + Uid.toString() + "\n" +
                "Train/Ori/Dest: " + train + "/" + ori + "/" + dest + "\n" +
                "Date: " + day + "/" + month + "/20" + year;
        tvNoKey.setText("");
        tvKey.setText(text);
    }

    //
    // generate the user private/public keypair into the Android Keystore
    //
    void genKeys() {
        String text;

        try {
            Calendar start = new GregorianCalendar();
            Calendar end = new GregorianCalendar();
            end.add(Calendar.YEAR, 20);            // 20 years validity
            KeyPairGenerator kgen = KeyPairGenerator.getInstance(Constants.KEY_ALGO, Constants.ANDROID_KEYSTORE);
            AlgorithmParameterSpec spec = new KeyPairGeneratorSpec.Builder(this)
                    .setKeySize(Constants.KEY_SIZE)
                    .setAlias(Constants.keyname)                                       // the name of the key (common name) to retrieve it
                    .setSubject(new X500Principal("CN=" + Constants.keyname))
                    .setSerialNumber(BigInteger.valueOf(Constants.CERT_SERIAL))       // a serial number to the public key certificate
                    .setStartDate(start.getTime())
                    .setEndDate(end.getTime())
                    .build();
            kgen.initialize(spec);
            KeyPair kp = kgen.generateKeyPair();
            pri = kp.getPrivate();                                         // private key in a Java class (PrivateKey)
            pub = kp.getPublic();                                          // the corresponding public key in a Java class (PublicKey)
            hasKey = true;
        }
        catch (Exception e) {
            tvNoKey.setText(e.getMessage());
            return;
        }
        findViewById(R.id.bt_send).setEnabled(hasKey);
        findViewById(R.id.bt_showkey).setEnabled(hasKey);
        findViewById(R.id.bt_genkey).setEnabled(!hasKey);
        tvNoKey.setText("");

        // show the public key in text
        byte[] modulus = ((RSAPublicKey)pub).getModulus().toByteArray();
        text = "Modulus (" + modulus.length + "):\n";
        text += byteArrayToHex(modulus) + "\n";
        text += "Public Exponent:\n";
        text += byteArrayToHex(((RSAPublicKey)pub).getPublicExponent().toByteArray()) + "\n";
        tvKey.setText(text);
    }

    //
    // generate a QR-code with the public key certificate in binary format (encoded aka DER)
    //
    void sendKey() {
        X509Certificate cert;

        try {
            KeyStore ks = KeyStore.getInstance(Constants.ANDROID_KEYSTORE);
            ks.load(null);
            KeyStore.Entry entry = ks.getEntry(Constants.keyname, null);
            if (entry != null) {
                cert = (X509Certificate)((KeyStore.PrivateKeyEntry)entry).getCertificate();
                tvKey.setText(cert.toString());
               /* Intent intent = new Intent(this, QRActivity.class);
                intent.putExtra("data", cert.getEncoded());
                startActivity(intent);*/
            }
        }
        catch (Exception e) {
            tvNoKey.setText(e.getMessage());
        }
    }

    //
    // show the public key certificate in multiple formats (binary(DER), base64 string, text)
    //
    void showKey() {
        try {
            KeyStore ks = KeyStore.getInstance(Constants.ANDROID_KEYSTORE);
            ks.load(null);
            KeyStore.Entry entry = ks.getEntry(Constants.keyname, null);
            if (entry != null) {
                X509Certificate cert = (X509Certificate)((KeyStore.PrivateKeyEntry)entry).getCertificate();
                byte[] encCert = cert.getEncoded();
                String strCert = cert.toString();
                String b64Cert = Base64.encodeToString(encCert, Base64.DEFAULT);

                String text = "(DER:"+ encCert.length + "):" + byteArrayToHex(encCert) + "\n\ncert(b64:" + b64Cert.length() + "): " + b64Cert + "\n\n" + strCert;
                tvKey.setText(text);

                // The base64 string with a header and a footer is the PEM format
                // printed in the Android Studio Log
                text = "-----BEGIN CERTIFICATE-----\n" + b64Cert +
                        "-----END CERTIFICATE-----\n";
                Log.d(TAG, text);
            }
        }
        catch (Exception e) {
            tvNoKey.setText(e.getMessage());
        }
    }

    // transform a byte[] into a string with hexadecimal digits
    String byteArrayToHex(byte[] ba) {
        StringBuilder sb = new StringBuilder(ba.length * 2);
        for(byte b: ba)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }
}
