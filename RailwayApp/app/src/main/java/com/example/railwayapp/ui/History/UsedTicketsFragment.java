package com.example.railwayapp.ui.History;

import android.content.Intent;
import android.media.Rating;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.railwayapp.Card;
import com.example.railwayapp.MyApplication;
import com.example.railwayapp.R;
import com.example.railwayapp.Review;
import com.example.railwayapp.Station;
import com.example.railwayapp.Stop;
import com.example.railwayapp.Ticket;
import com.example.railwayapp.Trip;
import com.example.railwayapp.User;
import com.example.railwayapp.ui.ReviewActivity;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsedTicketsFragment extends Fragment   implements AdapterView.OnItemClickListener{
    private MyApplication myApp;
    private View myFragment;
    private ListView usedTicketsList;
    private ArrayAdapter<Ticket> adapter;
    RelativeLayout loadingPanelUsedTickets;
    List<Ticket> usedTicketsDataProvider = new ArrayList<>();
    private Socket ioSocket;

    public UsedTicketsFragment() {
        //construtor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myApp = (MyApplication) getActivity().getApplicationContext();
        ioSocket = myApp.getSocket();

        setListening();

        myFragment = inflater.inflate(R.layout.fragment_used_tickets, container, false);

        //JUST FOR TESTING
        //addMockedData();
        //JUST FOR TESTING

        //GET DATA FROM SERVER
        myApp.getSocket().emit("getUsedTickets", myApp.getUser().getUuid());

        loadingPanelUsedTickets = myFragment.findViewById(R.id.loadingPanelUsedTickets);

        usedTicketsList = myFragment.findViewById(R.id.usedTicketsList);


        // Inflate the layout for this fragment
        return myFragment;
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int row, long l) {
        Intent intent = new Intent(getActivity().getApplicationContext(), ReviewActivity.class);
        intent.putExtra("SELECTED_TICKET", usedTicketsDataProvider.get(row).getUuid());
        startActivity(intent);

    }

    public void testOnClick(){
        usedTicketsList.setOnItemClickListener(this);
    }

    class UsedTicketsAdapter extends ArrayAdapter<Ticket> {

        UsedTicketsAdapter() {
            super(getActivity().getApplicationContext(), R.layout.used_ticket_row, usedTicketsDataProvider);
        }

        @Override
        public @NonNull
        View getView(int position, View convertView, @NonNull ViewGroup parent) {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-YYYY");
            View row = convertView;

            if (row == null) {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.used_ticket_row, parent, false);
            }

            Ticket t = usedTicketsDataProvider.get(position);

            ((TextView)row.findViewById(R.id.title)).setText(t.getOrigin().getName() + " > " + t.getDestination().getName());


            ((TextView)row.findViewById(R.id.date)).setText(t.getTravelDate()+" - "+t.getTravelTime());

            RatingBar rating = row.findViewById(R.id.ratingBar);

            if(t.getReview() == null)
            {
                rating.setVisibility(View.INVISIBLE);

            }
            else{
               System.out.println("rating " + t.getReview().getRating());
                rating.setRating(t.getReview().getRating());
                rating.setVisibility(View.VISIBLE);

            }


            return (row);
        }

    }

    private void setListening() {

        ioSocket.off("noUsedTickets");
        ioSocket.off("gotUsedTickets");

        ioSocket.on("noUsedTickets", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingPanelUsedTickets.setVisibility(View.GONE);
                        usedTicketsList.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

        ioSocket.on("gotUsedTickets", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingPanelUsedTickets.setVisibility(View.GONE);
                        usedTicketsList.setVisibility(View.VISIBLE);
                        try {
                            JSONArray usedTickets = new JSONArray(args[0].toString());
                            for (int i = 0; i < usedTickets.length(); i++) {
                                JSONObject tempTicket = usedTickets.getJSONObject(i);
                                usedTicketsDataProvider.add(new Ticket(tempTicket.getString("ticketUuid"),
                                        myApp.getStationByNumber(tempTicket.getInt("initialStationNumber")),
                                        myApp.getStationByNumber(tempTicket.getInt("finalStationNumber")),
                                        tempTicket.getString("date"),
                                        false,
                                        tempTicket.getString("time")));
                                /*Log.println(Log.ASSERT, "used tickets received:", "Name: "+tempTicket.getString("ticketUuid")+ ";\n"+
                                        myApp.getStationByNumber(tempTicket.getInt("initialStationNumber"))+ " -> "+
                                        myApp.getStationByNumber(tempTicket.getInt("finalStationNumber"))+";\n"+
                                        tempTicket.getString("ticketUuid"));*/
                            }
                            Collections.sort(usedTicketsDataProvider);
                            myApp.getUser().setUsedTickets(usedTicketsDataProvider);
                        }catch(Exception e){

                        }

                        //usedTicketsDataProvider = myApp.getUser().getUserUsedTickets(); //HERE

                        adapter = new UsedTicketsAdapter();
                        usedTicketsList.setAdapter(adapter);
                        testOnClick();
                    }
                });
            }
        });
    }

    public void addMockedData(){
        /*User user = new User("aee34950-70fe-11ea-bc55-0242ac130003", "Bruno Nascimento", "brunonascimento_2@gmail.com", "souburro", "Brasil","20-03-1985");
        myApp.setUser(user);

        //CREATE STATIONS
        Station station1 = new Station(1, "Trindade");
        Station station2 = new Station(2, "Bolhão");
        Station station3 = new Station(3, "Campo 24 de Agosto");
        Station station4 = new Station(4, "Heroísmo");
        Station station5 = new Station(5, "Campanhã");

        //CREATE TICKETS
        Ticket[] tickets = new Ticket[5];
        tickets[0] = new Ticket("183c5184-70f6-11ea-bc55-0242ac130003", station1, station2, new Date(), false);
        tickets[1] = new Ticket("183c567a-70f6-11ea-bc55-0242ac130003", station1, station5, new Date(), false);
        tickets[2] = new Ticket("183c579c-70f6-11ea-bc55-0242ac130003", station3, station5, new Date(), true);
        tickets[2].setReview(new Review(3.5f, " vai funceminar"));
        tickets[3] = new Ticket("183c586e-70f6-11ea-bc55-0242ac130003", station4, station2, new Date(), true);
        tickets[4] = new Ticket("183c5940-70f6-11ea-bc55-0242ac130003", station2, station4, new Date(), false);

        //ADD TICKETS TO USER
        for(int i=0; i<tickets.length; i++){
            myApp.getUser().addTicket(tickets[i]);
        }*/
    }
}
