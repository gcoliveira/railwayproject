package com.example.railwayapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.railwayapp.Constants;
import com.example.railwayapp.MyApplication;
import com.example.railwayapp.R;
import com.example.railwayapp.Ticket;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.Signature;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.UUID;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import javax.crypto.Cipher;

public class TicketActivity extends AppCompatActivity {

    private MyApplication myApp;
    private SimpleDateFormat formatDate;
    private Ticket selectedTicket;
    private ImageView qrCode;
    private RelativeLayout loadingPanelTicketView;
    final static String CH_SET = "ISO-8859-1";
    final static int DIMENSION = 1000;
    public int limit = 0;
    Socket ioSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        formatDate = new SimpleDateFormat("dd-MM-YYYY");

        //get info passed by the previous screen
        myApp = (MyApplication) getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();

        String selectedTicketUuid = getIntent().getStringExtra("SELECTED_TICKET");




        selectedTicket = myApp.getUser().getTicketByUuid(selectedTicketUuid);

        //set travel info on screen
        ((TextView) findViewById(R.id.originStation)).setText(selectedTicket.getOrigin().getName());
        ((TextView) findViewById(R.id.destinationStation)).setText(selectedTicket.getDestination().getName());
        ((TextView) findViewById(R.id.dateTravel)).setText(selectedTicket.getTravelDate() + " - " + selectedTicket.getTravelTime());

        qrCode = findViewById(R.id.qrCode);
        loadingPanelTicketView = findViewById(R.id.loadingPanelTicketView);

        qrCode.setVisibility(View.GONE);

        ioSocket.emit("getEncTicketAndSignature", myApp.getUser().getUuid(), selectedTicketUuid, myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getPubKey()));
        //generateQrCode();
    }


    //public void showInQRCode(String uuuid, String tuuid, String origin, String dest, String time, String date, byte[] signature){
    public void showInQRCode(byte[] clearTicket, byte[] signature){
        ByteBuffer ticket;    // useful class to accumulate bytes
        byte[] encTicket;

        // encrypt the ticket with the private key (obtain a 64 byte result)
        try {
            Cipher cipher = Cipher.getInstance(Constants.ENC_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, myApp.getKeyManager().getPriKey());
            encTicket = cipher.doFinal(clearTicket);

            Log.println(Log.ASSERT, "processQRCode", "byte encTicket:"+encTicket);
            Log.println(Log.ASSERT, "processQRCode", "byte signature:"+signature);
            Log.println(Log.ASSERT, "processQRCode", "byte ticket LENGTH:"+encTicket.length);
            Log.println(Log.ASSERT, "processQRCode", "byte SIG LENGTH:"+signature.length);
            // display encryptd ticket + signature
        }
        catch (Exception e) {
            return;
        }

        // send E(pri, ticket) || S(pri, ticket) to be shown as a QR-code
        /*byte[] all_ticket = new byte[encTicket.length + signature.length];
        System.arraycopy(encTicket, 0, all_ticket, 0, encTicket.length);
        System.arraycopy(signature, 0, all_ticket, encTicket.length, signature.length);*/


        //String encryptedTicked = new String(encTicket, StandardCharsets.ISO_8859_1);
        //String signatureString = new String(signature, StandardCharsets.ISO_8859_1);
        //String qr_content = selectedTicket.getUuid() + encryptedTicked + signatureString;


        byte[] byteUuid = selectedTicket.getUuid().getBytes(StandardCharsets.ISO_8859_1);


        String uuidtest = new String(byteUuid, StandardCharsets.ISO_8859_1);
        String tickettest = new String(encTicket, StandardCharsets.ISO_8859_1);
        String signaturetest = new String(signature, StandardCharsets.ISO_8859_1);

        Log.println(Log.ASSERT, "byte sizes", "uuid:"+byteUuid.length);
        Log.println(Log.ASSERT, "byte sizes", "ticket:"+encTicket.length);
        Log.println(Log.ASSERT, "byte sizes", "signature:"+signature.length);
        Log.println(Log.ASSERT, "byte content", "uuid:"+uuidtest);
        Log.println(Log.ASSERT, "byte content", "ticket:"+tickettest);
        Log.println(Log.ASSERT, "byte content", "signature:"+signaturetest);

        byte[] all_ticket = new byte[byteUuid.length + encTicket.length + signature.length];
        System.arraycopy(byteUuid, 0, all_ticket, 0, byteUuid.length);
        System.arraycopy(encTicket, 0, all_ticket, byteUuid.length, encTicket.length);
        System.arraycopy(signature, 0, all_ticket, byteUuid.length + encTicket.length, signature.length);

        String encoded = Base64.encodeToString(all_ticket, Base64.DEFAULT);

        String test = new String(all_ticket, StandardCharsets.ISO_8859_1);
        String qr_content = encoded;


        Log.println(Log.ASSERT, "full string", "qr_content:"+qr_content);
        Log.println(Log.ASSERT, "full byte sizes", "qr_content length:"+qr_content.length());


        /*Log.println(Log.ASSERT, "AI MAEZINHA", "string encTicket:"+encryptedTicked);
        Log.println(Log.ASSERT, "AI MAEZINHA", "string signature:"+signatureString);
        Log.println(Log.ASSERT, "processQRCode", "string ticket LENGTH:"+encryptedTicked.length());
        Log.println(Log.ASSERT, "processQRCode", "string SIG LENGTH:"+signatureString.length());*/

        Log.println(Log.ASSERT, "QR_content", qr_content + "; size: "+qr_content.length());


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    Bitmap bm = encodeAsBitmap(qr_content);

                    if (bm != null) {
                        qrCode.setImageBitmap(bm);
                        loadingPanelTicketView.setVisibility(View.GONE);
                        qrCode.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });


        /*      WILL BE USED IN THE  INSPECTOR
        String lines[] = qr_content.split("\\r?\\n");

        for(String s:lines){
            Log.println(Log.ASSERT, "check data", s);
        }


        String firstUUID = lines[0];
        String encriptedStuff = lines[1];
        String encriptedStuff2 = lines[2];

        byte[] lole = encriptedStuff.getBytes(StandardCharsets.ISO_8859_1);
        byte[] siglole = encriptedStuff2.getBytes(StandardCharsets.ISO_8859_1);



        Log.println(Log.ASSERT, "AI MAEZINHA", "LENGTH:"+lole.length);
        desyncriptThings(lole, siglole);

        */



    }


    Bitmap encodeAsBitmap(String str) {
        BitMatrix result;

        Hashtable<EncodeHintType, String> hints = new Hashtable<>();
        hints.put(EncodeHintType.CHARACTER_SET, CH_SET);
        try {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, DIMENSION, DIMENSION, hints);
        }
        catch (Exception exc) {
            //runOnUiThread(()->errorTv.setText(exc.getMessage()));
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int colorPrimary = getResources().getColor(R.color.design_default_color_on_surface, null);
        int colorWhite = getResources().getColor(R.color.design_default_color_on_primary, null);
        int[] pixels = new int[w * h];
        for (int line = 0; line < h; line++) {
            int offset = line * w;
            for (int col = 0; col < w; col++) {
                pixels[offset + col] = result.get(col, line) ? colorPrimary : colorWhite;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        return bitmap;
    }

    private void setListening() {

        ioSocket.off("EncTicketAndSignature");
        ioSocket.off("returnNoReview");

        ioSocket.on("EncTicketAndSignature", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                //String signatureString = new String((byte[])args[2], StandardCharsets.UTF_8);
                //Log.println(Log.ASSERT, "testing bytebuffers", args[0].toString() + "with size:"+shit.length+"with limit:"+ args[1] + "\nSignature:"+ signatureString);
                //Log.println(Log.ASSERT, "testing serverpubkey", myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getServerPubKey()));
                //Log.println(Log.ASSERT, "EncTicketAndSignature", "We got encrypted ticket + limit + signature");
                try {
                    Cipher cipher = Cipher.getInstance(Constants.ENC_ALGO);
                    cipher.init(Cipher.DECRYPT_MODE, myApp.getKeyManager().getPriKey());
                    //byte[] cypher_bytes = Base64.decode((byte[])args[0], Base64.DEFAULT);
                    byte[] clearTicket = cipher.doFinal((byte[])args[0]);


                    limit = (int)args[1];

                    Log.println(Log.ASSERT, "LIMIT VALUE", "LIMIT: "+limit);



                    ByteBuffer bTicket = ByteBuffer.wrap(clearTicket, 0, (int)args[1]);
                    byte[] firstuuid = new byte[36];
                    byte[] seconduuid = new byte[36];
                    bTicket.get(firstuuid, 0, 36);
                    bTicket.get(seconduuid, 0, 36);
                    String uuuid = new String(firstuuid, StandardCharsets.UTF_8);
                    String tuuid = new String(seconduuid, StandardCharsets.UTF_8);
                    Log.println(Log.ASSERT, "testing bytebuffers", uuuid);
                    Log.println(Log.ASSERT, "testing bytebuffers", tuuid);

                    String origin = new String(new byte[] { bTicket.get() });                                 // Origin Station nr (1..5)
                    String dest = new String(new byte[] { bTicket.get() });                                     // Destination Station nr (1..5)

                    byte[] timeByte = new byte[5];
                    bTicket.get(timeByte, 0, 5);
                    String time = new String(timeByte, StandardCharsets.UTF_8);

                    byte[] dateByte = new byte[bTicket.array().length - bTicket.position()];
                    bTicket.get(dateByte, 0, bTicket.array().length - bTicket.position());
                    String date = new String(dateByte, StandardCharsets.UTF_8);



                    Log.println(Log.ASSERT, "testing bytebuffers", "Origin:" + origin + "\nDestination: " + dest + "\nTime: "+time+" Date: "+date);

                    byte[] signature = (byte[])args[2];

                    Signature sg = Signature.getInstance(Constants.SIGN_ALGO);
                    sg.initVerify(myApp.getKeyManager().getServerPubKey());
                    sg.update(bTicket.array());
                    boolean verified = sg.verify(signature);

                    Log.println(Log.ASSERT, "Verified:", " "+verified);

                    if(verified){
                        //showInQRCode(uuuid, tuuid, origin, dest, time, date, signature);

                        Log.println(Log.ASSERT, "AI MAEZINHA", "LENGTH:"+clearTicket.length);
                        showInQRCode(clearTicket, signature);
                    }else{
                        //finish();
                    }


                }
                catch (Exception e) {
                    Log.println(Log.ASSERT, "Error ENCRYPTION", e.toString());
                    e.printStackTrace();
                    return;
                }
            }
        });

        ioSocket.on("returnNoReview", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ;
            }
        });



    }


    public void desyncriptThings(byte[] encryptedThing, byte[] signature){
        try {
            Cipher cipher = Cipher.getInstance(Constants.ENC_ALGO);
            cipher.init(Cipher.DECRYPT_MODE, myApp.getKeyManager().getPubKey());
            //byte[] cypher_bytes = Base64.decode((byte[])args[0], Base64.DEFAULT);
            Log.println(Log.ASSERT, "AI MAEZINHA", "LENGTH:"+encryptedThing.length);
            byte[] clearTicket = cipher.doFinal(encryptedThing);



            ByteBuffer bTicket = ByteBuffer.wrap(clearTicket, 0, limit);
            byte[] firstuuid = new byte[36];
            byte[] seconduuid = new byte[36];
            bTicket.get(firstuuid, 0, 36);
            bTicket.get(seconduuid, 0, 36);
            String uuuid = new String(firstuuid, StandardCharsets.UTF_8);
            String tuuid = new String(seconduuid, StandardCharsets.UTF_8);
            Log.println(Log.ASSERT, "testing bytebuffers", uuuid);
            Log.println(Log.ASSERT, "testing bytebuffers", tuuid);

            String origin = new String(new byte[] { bTicket.get() });                                 // Origin Station nr (1..5)
            String dest = new String(new byte[] { bTicket.get() });                                     // Destination Station nr (1..5)

            byte[] timeByte = new byte[5];
            bTicket.get(timeByte, 0, 5);
            String time = new String(timeByte, StandardCharsets.UTF_8);

            byte[] dateByte = new byte[bTicket.array().length - bTicket.position()];
            bTicket.get(dateByte, 0, bTicket.array().length - bTicket.position());
            String date = new String(dateByte, StandardCharsets.UTF_8);



            Log.println(Log.ASSERT, "testing bytebuffers", "Origin:" + origin + "\nDestination: " + dest + "\nTime: "+time+" Date: "+date);

            Signature sg = Signature.getInstance(Constants.SIGN_ALGO);
            sg.initVerify(myApp.getKeyManager().getServerPubKey());
            sg.update(bTicket.array());
            boolean verified = sg.verify(signature);

            Log.println(Log.ASSERT, "Verified:", " "+verified);

            if(verified){
                //showInQRCode(uuuid, tuuid, origin, dest, time, date, signature);
                //showInQRCode(clearTicket, signature);
            }else{
                //finish();
            }


        }
        catch (Exception e) {
            Log.println(Log.ASSERT, "Error ENCRYPTION", e.toString());
            e.printStackTrace();
            return;
        }
    }

}
