package com.example.railwayapp;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String uuid, name, nationality, birthdate, email;
    private String password;
    private List<Ticket> usedTickets, unusedTickets;

    public User(String uuid, String name, String email, String password, String nationality, String birthdate) {
        this.uuid = uuid;
        this.name = name;
        this.nationality = nationality;
        this.birthdate = birthdate;
        this.password = password;
        this.email = email;
        usedTickets = new ArrayList<>();
        unusedTickets = new ArrayList<>();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsedTickets(List<Ticket> usedTickets){
        this.usedTickets = usedTickets;
    }

    public void addUsedTicket(Ticket t){
        usedTickets.add(t);
    }

    public List<Ticket> getUsedTickets() {
        return usedTickets;
    }


    public Ticket getUsedTicketByUuid(String uuid){
        Ticket output = null;
        for(Ticket t : usedTickets){
            if(t.getUuid().equalsIgnoreCase(uuid)){
                output = t;
            }
        }

        return output;
    }

    public void setUnusedTickets(List<Ticket> unusedTickets){
        this.unusedTickets = unusedTickets;
    }

    public void addUnusedTicket(Ticket t){
        unusedTickets.add(t);
    }

    public List<Ticket> getUnusedTickets() {
        return unusedTickets;
    }


    public Ticket getUnusedTicketByUuid(String uuid){
        Ticket output = null;
        for(Ticket t : unusedTickets){
            if(t.getUuid().equalsIgnoreCase(uuid)){
                output = t;
            }
        }

        return output;
    }
    public Ticket getTicketByUuid(String uuid){
        for(Ticket t : unusedTickets){
            if(t.getUuid().equalsIgnoreCase(uuid)){
                return t;
            }
        }

        for(Ticket t : usedTickets){
            if(t.getUuid().equalsIgnoreCase(uuid)){
                return t;
            }
        }

        return null;
    }

    @Override
    public String toString(){
        return "name: " + name + " ["+uuid+"];\nemail: "+email+";\nnationality: "+nationality+";\nbirthday: "+birthdate+";";
    }


}
