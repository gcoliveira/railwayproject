package com.example.railwayapp.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.railwayapp.Card;
import com.example.railwayapp.MyApplication;
import com.example.railwayapp.R;
import com.example.railwayapp.Review;
import com.example.railwayapp.Ticket;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ReviewActivity extends AppCompatActivity {

    private RatingBar ratingBar;
    private EditText descriptionText;
    private Ticket selectedTicket;
    private Button finishReview;
    private Review review;
    private MyApplication myApp;
    private TextView textView2;
    private Socket ioSocket;
    private RelativeLayout loadingPanelReview, reviewDateRL;
    private LinearLayout reviewLL, reviewRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-YYYY");
        MyApplication myApp = (MyApplication) getApplicationContext();
        String selectedTicketUuid = getIntent().getStringExtra("SELECTED_TICKET");
        selectedTicket = myApp.getUser().getTicketByUuid(selectedTicketUuid);
        ((TextView) findViewById(R.id.originStation)).setText(selectedTicket.getOrigin().getName());
        ((TextView) findViewById(R.id.destinationStation)).setText(selectedTicket.getDestination().getName());
        ((TextView) findViewById(R.id.travelDate)).setText(selectedTicket.getTravelTime() + " - " + selectedTicket.getTravelDate());

        myApp = (MyApplication) getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();

        ioSocket.emit("isTicketReviewed", selectedTicketUuid);

        loadingPanelReview = findViewById(R.id.loadingPanelReview);
        reviewRL = findViewById(R.id.reviewRL);
        reviewDateRL = findViewById(R.id.reviewDateRL);
        ratingBar = findViewById(R.id.ratingBar);
        reviewLL = findViewById(R.id.reviewLL);
        textView2 = findViewById(R.id.textView2);

        descriptionText = findViewById(R.id.description);

        finishReview = findViewById(R.id.finishReview);




        if(selectedTicket.getReview() != null){
            ratingBar.setRating(selectedTicket.getReview().getRating());
            descriptionText.setText(selectedTicket.getReview().getDescription());
        }

        review = new Review();
    }

    private void setListening() {

        ioSocket.off("returnReview");
        ioSocket.off("returnNoReview");
        ioSocket.off("reviewCreated");

        ioSocket.on("returnReview", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                fillReviewForm(Float.parseFloat((String)args[0]), (String)args[1]);
            }
        });

        ioSocket.on("returnNoReview", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                showReviewForm();
            }
        });

        ioSocket.on("reviewCreated", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                finish();
            }
        });



    }

    private void showReviewForm() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                finishReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    float rating = ratingBar.getRating();
                    String description = descriptionText.getText().toString();

                    if(description.isEmpty() || description == null){
                        descriptionText.setError("Can't be empty");
                        return;
                    }
                    ratingBar.setEnabled(false);
                    descriptionText.setEnabled(false);
                    finishReview.setEnabled(false);

                    ioSocket.emit("createReview", selectedTicket.getUuid(), String.valueOf(rating), description);
                }
                });
                makeFormVisible();
            }

        });

    }

    private void fillReviewForm(float rating, String description) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView2.setText("You rated your experience");
                ratingBar.setRating(rating);
                descriptionText.setText(description);
                ratingBar.setEnabled(false);
                descriptionText.setEnabled(false);
                finishReview.setText("Return");
                finishReview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                makeFormVisible();
            }
        });
    }

    private void makeFormVisible() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reviewRL.setVisibility(View.VISIBLE);
                reviewDateRL.setVisibility(View.VISIBLE);
                reviewLL.setVisibility(View.VISIBLE);
                loadingPanelReview.setVisibility(View.GONE);
            }
        });
    }


}

