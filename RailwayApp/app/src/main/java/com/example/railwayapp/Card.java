package com.example.railwayapp;

public class Card {
    String type, validity, number;

    public Card(String type, String validity, String number) {
        this.type = type;
        this.validity = validity;
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
