package com.example.railwayapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BuyTicketActivity extends AppCompatActivity {

    TextView destinationStationTitle, originStationTitle, destinationStationTextView, originStationTextView, originStationHourTextView, destinationStationHourTextView;
    LinearLayout stationsInBetweenLinearLayout;
    MyApplication myApp;
    Spinner cardChoiceSpinner;
    Button confirmPurchaseButton, addCardIfNoCard;
    Stop initStop, finalStop;
    RelativeLayout loadingPanelBuyTicket;
    Socket ioSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_ticket);
        Intent intent = getIntent();



        myApp = (MyApplication) getApplicationContext();

        ioSocket = myApp.getSocket();
        setListening();

        loadingPanelBuyTicket = findViewById(R.id.loadingPanelBuyTicket);

        destinationStationTitle = findViewById(R.id.destinationStationTitle);
        destinationStationTitle.setText(intent.getStringExtra("destination_station"));
        originStationTitle = findViewById(R.id.originStationTitle);
        originStationTitle.setText(intent.getStringExtra("origin_station"));

        destinationStationTextView = findViewById(R.id.destinationStationTextView);
        destinationStationTextView.setText(intent.getStringExtra("destination_station"));
        destinationStationHourTextView = findViewById(R.id.destinationStationHourTextView);
        destinationStationHourTextView.setText(intent.getStringExtra("destination_station_hour"));

        originStationTextView = findViewById(R.id.originStationTextView);
        originStationTextView.setText(intent.getStringExtra("origin_station"));
        originStationHourTextView = findViewById(R.id.originStationHourTextView);
        originStationHourTextView.setText(intent.getStringExtra("origin_station_hour"));

        initStop = myApp.getStopById(intent.getIntExtra("origin_stop_id", 0));
        finalStop = myApp.getStopById(intent.getIntExtra("destination_stop_id", 0));

        confirmPurchaseButton = findViewById(R.id.confirmPurchaseButton);
        addCardIfNoCard = findViewById(R.id.addCardIfNoCard);

        cardChoiceSpinner = findViewById(R.id.cardChoiceSpinner);
        //cardChoiceSpinner.setEnabled(false);

        checkCreditCardList();

        addCardIfNoCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addCard = new Intent(getApplicationContext(), AddCreditCardActivity.class);
                startActivityForResult(addCard, 2);
            }
        });


        stationsInBetweenLinearLayout = findViewById(R.id.stationsInBetweenLinearLayout);

        Station originStation = myApp.getStationByName(intent.getStringExtra("origin_station"));
        Station destinationStation = myApp.getStationByName(intent.getStringExtra("destination_station"));

        int stationsInBetween;
        boolean originBiggerThanDestination = false;

        if(originStation.getStationNumber() > destinationStation.getStationNumber()){
            stationsInBetween = originStation.getStationNumber() - destinationStation.getStationNumber();
            originBiggerThanDestination = true;
        }else{
            stationsInBetween = destinationStation.getStationNumber() - originStation.getStationNumber();
            originBiggerThanDestination = false;
        }

        stationsInBetweenLinearLayout.addView(createImageViewWithDrawable(getDrawable(R.drawable.ic_arrow_down)));

        for(int i = 1; i < stationsInBetween; i++){
            Station tempStation = null;
            Stop tempStop = null;

            //IN BETWEEN STATION

            LinearLayout llInBetweenStation = new LinearLayout(getApplicationContext());
            llInBetweenStation.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            llInBetweenStation.setGravity(Gravity.CENTER);

            LinearLayout inBetweenStationImgll = createImageViewWithDrawableAndWeight(getDrawable(R.drawable.ic_train_inbetweenstop), 4);
            inBetweenStationImgll.setGravity(Gravity.RIGHT);


            if(originBiggerThanDestination) {
                tempStation = myApp.getStationByNumber(originStation.getStationNumber() - i);

            }else {
                tempStation = myApp.getStationByNumber(originStation.getStationNumber() + i);
            }
            tempStop = myApp.getStopById(initStop.getStopId() + i);

            Log.println(Log.ASSERT, "wat", "mt station:" + tempStation.getName()+"; number:"+stationsInBetween);

            TextView tvInBetweenStation = new TextView(getApplicationContext());
            LinearLayout.LayoutParams lllp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 5);
            tvInBetweenStation.setLayoutParams(lllp);
            tvInBetweenStation.setGravity(Gravity.CENTER);

            tvInBetweenStation.setText(tempStation.getName());
            tvInBetweenStation.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);

            TextView tvInBetweenStationHour = new TextView(getApplicationContext());
            LinearLayout.LayoutParams lllpHour = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 4);
            tvInBetweenStationHour.setLayoutParams(lllp);

            tvInBetweenStationHour.setText(tempStop.getStopTime());
            tvInBetweenStationHour.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);

            llInBetweenStation.addView(inBetweenStationImgll);
            llInBetweenStation.addView(tvInBetweenStation);
            llInBetweenStation.addView(tvInBetweenStationHour);

            stationsInBetweenLinearLayout.addView(llInBetweenStation);
            stationsInBetweenLinearLayout.addView(createImageViewWithDrawable(getDrawable(R.drawable.ic_arrow_down)));

        }

        String dataFromIntent = ("Origin station:"+ intent.getStringExtra("origin_station") +
                "\nDestination station:"+ intent.getStringExtra("destination_station") +
                "\nDate:"+ intent.getStringExtra("date") +
                "\nPrice:"+ intent.getStringExtra("price") );


        confirmPurchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //VER SE O CARTAO AINDA FUNCIONA

                cardChoiceSpinner.setEnabled(false);
                confirmPurchaseButton.setEnabled(false);
                loadingPanelBuyTicket.setVisibility(View.VISIBLE);

                myApp.getSocket().emit("buyTicket", myApp.getUser().getUuid(), originStation.getStationNumber(), destinationStation.getStationNumber(),
                        initStop.getStopId(), finalStop.getStopId(), intent.getStringExtra("date"), intent.getStringExtra("price"), initStop.getStopTime());
            }
        });





        //ADD CARD SELECTION
        //ADD CONFIRM BUTTON
        //ADD BACK BUTTON

        //ADD WAITING ANIMATION

        //ADD CONFIRMED MESSAGE AND BACK BUTTON


    }

    private void checkCreditCardList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<Card> creditCards = new ArrayList<>();
                creditCards = myApp.getCreditCards();
                if(creditCards.size() <= 0){
                    cardChoiceSpinner.setEnabled(false);
                    confirmPurchaseButton.setEnabled(false);
                    addCardIfNoCard.setVisibility(View.VISIBLE);
                }else{

                    cardChoiceSpinner.setEnabled(true);
                    confirmPurchaseButton.setEnabled(true);
                    addCardIfNoCard.setVisibility(View.GONE);
                }


                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item);
                for(Card c : creditCards) {
                    adapter.add("["+c.getType()+"] "+c.getNumber() );
                }
                adapter.setDropDownViewResource(R.layout.spinner_item);
                cardChoiceSpinner.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            Log.println(Log.ASSERT, "REQUEST CODE 1", "BACK FROM CONFIRMED PURCHASE");
            if (resultCode == RESULT_OK) {

                Intent backToHomePage = new Intent();
                setResult(RESULT_OK, backToHomePage);
                finish();
            }
        }else if(requestCode == 2){
            Log.println(Log.ASSERT, "REQUEST CODE 2", "BACK FROM NEW CARD");
            checkCreditCardList();
        }
    }

    public LinearLayout createImageViewWithDrawableAndWeight(Drawable drawable, int weight){
        //ARROW IMAGE
        LinearLayout llImgArrowDown = new LinearLayout(getApplicationContext());
        llImgArrowDown.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, weight));
        llImgArrowDown.setGravity(Gravity.RIGHT);

        ImageView imgViewArrowDown = new ImageView(getApplicationContext());
        imgViewArrowDown.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        imgViewArrowDown.setBackground(drawable);

        llImgArrowDown.addView(imgViewArrowDown);

        return llImgArrowDown;
    }



    public LinearLayout createImageViewWithDrawable(Drawable drawable){
        //ARROW IMAGE
        LinearLayout llImgArrowDown = new LinearLayout(getApplicationContext());
        llImgArrowDown.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        llImgArrowDown.setGravity(Gravity.CENTER);

        ImageView imgViewArrowDown = new ImageView(getApplicationContext());
        imgViewArrowDown.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        imgViewArrowDown.setBackground(drawable);

        llImgArrowDown.addView(imgViewArrowDown);

        return llImgArrowDown;
    }


    private void noSeatsLeft(String err){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cardChoiceSpinner.setEnabled(true);
                confirmPurchaseButton.setEnabled(true);
                confirmPurchaseButton.setText(err);
                loadingPanelBuyTicket.setVisibility(View.GONE);

            }
        });
    }

    private void launchConfirmationActivity(){
        Intent confirmPurchase = new Intent(this, ConfirmationActivity.class);
        startActivityForResult(confirmPurchase, 1);
        System.out.println("PURCHASE WAS A SUCCESS");
        finish();

    }

    private void setListening() {


        ioSocket.off("ticketPurchaseComplete");
        ioSocket.off("ticketPurchaseError");


        ioSocket.on("ticketPurchaseComplete", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                launchConfirmationActivity();
            }
        });

        ioSocket.on("ticketPurchaseError", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.println(Log.ASSERT, "ticketPurchaseError", (String)args[0]);
                noSeatsLeft((String)args[0]);
            }
        });

    }




}
