package com.example.railwayapp;

public class Constants {
    static final int KEY_SIZE = 1024;
    static final String ANDROID_KEYSTORE = "AndroidKeyStore";
    static final String KEY_ALGO = "RSA";                // cryptography family
    static public final String SIGN_ALGO = "SHA256WithRSA";     // signature algorithm
    static final int CERT_SERIAL = 12121212;            // certificate serial name (any one does the job)
    static public final String ENC_ALGO = "RSA/ECB/PKCS1Padding";   // encrypt/decrypt algorithm
    static String keyname = "myUserKey";                    // common name in the KeyStore and public key certificate
}
