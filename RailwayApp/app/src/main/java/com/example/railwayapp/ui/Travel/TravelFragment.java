package com.example.railwayapp.ui.Travel;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.railwayapp.BuyTicketActivity;
import com.example.railwayapp.MainActivity;
import com.example.railwayapp.MyApplication;
import com.example.railwayapp.R;
import com.example.railwayapp.Station;
import com.example.railwayapp.Stop;
import com.example.railwayapp.Trip;

import java.math.BigDecimal;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class TravelFragment extends Fragment {

    private TravelViewModel travelViewModel;
    private Spinner oriStation, destStation;
    private DatePicker travelDatePicker;
    private RelativeLayout destRL, oriRL, loadingPanelTravel;
    private TableLayout tripsTableLayout;
    private Button seeTicketsButton, backToSelectionButton;
    private MyApplication myApp;
    private TextView dateSelectedTextView;
    private TableRow topTableRow;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        travelViewModel = ViewModelProviders.of(this).get(TravelViewModel.class);
        View root = inflater.inflate(R.layout.fragment_travel, container, false);



        myApp = (MyApplication) getActivity().getApplicationContext();

        oriStation = root.findViewById(R.id.originStationTravelSpinner);
        destStation = root.findViewById(R.id.destinationStationTravelSpinner);
        destRL = root.findViewById(R.id.destinationRelativeLayout);
        oriRL = root.findViewById(R.id.originRelativeLayout);
        loadingPanelTravel = root.findViewById(R.id.loadingPanelTravel);
        travelDatePicker = root.findViewById(R.id.travelDatePicker);
        seeTicketsButton = root.findViewById(R.id.seeTicketsButton);
        tripsTableLayout = root.findViewById(R.id.tripsTableLayout);
        topTableRow = root.findViewById(R.id.topTableRow);

        backToSelectionButton = root.findViewById(R.id.backToSelectionButton);
        dateSelectedTextView = root.findViewById(R.id.dateSelectedTextView);


        updateAdapters();



        destStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(id<5){
                    destRL.setPadding(0, 0, 0, 0);
                }
                if(oriStation.getSelectedItemId() == id){
                    ((TextView)destStation.getSelectedView()).setError("Destination and Origin can't be the same");
                    ((TextView)oriStation.getSelectedView()).setError("Destination and Origin can't be the same");
                }else{
                    ((TextView)destStation.getSelectedView()).setError(null);
                    ((TextView)oriStation.getSelectedView()).setError(null);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.println(Log.ASSERT, "nothing selected", "NOTHING SELECTED");
            }
        });

        oriStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(id<5){
                    oriRL.setPadding(0, 0, 0, 0);
                }
                if(destStation.getSelectedItemId() == id){
                    ((TextView)destStation.getSelectedView()).setError("Destination and Origin can't be the same");
                    ((TextView)oriStation.getSelectedView()).setError("Destination and Origin can't be the same");
                }else{
                    ((TextView)destStation.getSelectedView()).setError(null);
                    ((TextView)oriStation.getSelectedView()).setError(null);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.println(Log.ASSERT, "nothing selected", "NOTHING SELECTED");
            }
        });


        seeTicketsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(oriStation.getSelectedItemId() >= 5){
                    ((TextView)oriStation.getSelectedView()).setError("Select a station");
                } else if(destStation.getSelectedItemId() > 5){
                    ((TextView)destStation.getSelectedView()).setError("Select a station");
                }else{
                    travelDatePicker.setVisibility(View.GONE);
                    oriRL.setVisibility(View.GONE);
                    destRL.setVisibility(View.GONE);
                    seeTicketsButton.setVisibility(View.GONE);
                    dateSelectedTextView.setVisibility(View.VISIBLE);
                    backToSelectionButton.setVisibility(View.VISIBLE);

                    tripsTableLayout.setVisibility(View.VISIBLE);

                    List<Station> stations = myApp.getStations();

                    String oriString = ((TextView)oriStation.getSelectedView()).getText().toString();
                    String destString = ((TextView)destStation.getSelectedView()).getText().toString();

                    Station oriStation= null, destStation = null;


                    for (Station s: stations) {
                        if(s.getName().equalsIgnoreCase(oriString)){
                            oriStation = s;
                        }
                        if(s.getName().equalsIgnoreCase(destString)){
                            destStation = s;
                        }
                    }
                    if(oriStation != null && destStation != null){

                        List<Stop> stops = new ArrayList<>();

                        if(oriStation.getStationNumber() < destStation.getStationNumber()){
                            stops = myApp.getStopsInStationMovingUp(oriStation.getStationNumber());
                        }else{
                            stops = myApp.getStopsInStationMovingDown(oriStation.getStationNumber());
                        }

                        int pickedYear = travelDatePicker.getYear();
                        int pickedMonth = travelDatePicker.getMonth()+1;
                        int pickedDay = travelDatePicker.getDayOfMonth();

                        dateSelectedTextView.setText("Tickets for date: "+pickedDay+"/"+pickedMonth+"/"+pickedYear);

                        int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                        int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
                        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

                        if(pickedDay == currentDay && pickedMonth == currentMonth && pickedYear == currentYear) {

                            int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                            int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);

                            List<Stop> finalStops = new ArrayList<>();
                            for (Stop s : stops) {
                                if (s.getStopTimeHour() > currentHour) {
                                    finalStops.add(s);
                                } else if (s.getStopTimeHour() == currentHour && s.getStopTimeMinute() > currentMinute) {
                                    finalStops.add(s);
                                }
                            }
                            stops = finalStops;
                        }


                        Log.println(Log.ASSERT, "Stops left:", stops.toString());


                        for (Stop s: stops) {

                            Trip aux = myApp.getTripWithId(s.getTripId());
                            Stop finalStop = aux.getStopWithStationNumber(destStation.getStationNumber());


                            int auxPrice = finalStop.getStopId() - s.getStopId();
                            BigDecimal bd1 = new BigDecimal("0.60");
                            BigDecimal bd2 = new BigDecimal(String.valueOf(auxPrice));
                            bd1 = bd1.multiply(bd2);
                            BigDecimal finalBd = bd1;

                            TableRow tr = new TableRow(getActivity().getApplicationContext());
                            TableRow.LayoutParams trlp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                            trlp.setMargins(10, 100, 10, 10);
                            tr.setLayoutParams(trlp);
                            tr.setBackground(getContext().getDrawable(R.drawable.table_normalcell_border));
                            tr.setPadding(5, 5, 5, 5);

                            tr.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent buyIntent = new Intent(getActivity().getApplicationContext(), BuyTicketActivity.class);
                                    buyIntent.putExtra("origin_station", oriString);
                                    buyIntent.putExtra("destination_station", destString);
                                    buyIntent.putExtra("origin_station_hour", s.getStopTime());
                                    buyIntent.putExtra("destination_station_hour", finalStop.getStopTime());
                                    buyIntent.putExtra("origin_stop_id", s.getStopId());
                                    buyIntent.putExtra("destination_stop_id", finalStop.getStopId());
                                    buyIntent.putExtra("date", pickedDay+"/"+pickedMonth+"/"+pickedYear);
                                    buyIntent.putExtra("price", finalBd.toString());
                                    startActivityForResult(buyIntent, 1);
                                    //CREATE NEW ACTIVITY TO BUY

                                }
                            });


                            // LIINEAR LAYOUT 1 / 3
                            LinearLayout llImgViewOrigin = new LinearLayout(getActivity().getApplicationContext());
                            llImgViewOrigin.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 3));
                            llImgViewOrigin.setOrientation(LinearLayout.VERTICAL);
                            llImgViewOrigin.setGravity(Gravity.CENTER);

                            LinearLayout llImg1 = new LinearLayout(getActivity().getApplicationContext());
                            llImg1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 3));
                            llImg1.setGravity(Gravity.CENTER);

                            ImageView imgViewOrigin = new ImageView(getActivity().getApplicationContext());
                            imgViewOrigin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT ));
                            imgViewOrigin.setBackground(getContext().getDrawable(R.drawable.ic_origin));
                            imgViewOrigin.setScaleType(ImageView.ScaleType.CENTER);
                            imgViewOrigin.setPadding(20, 20, 20, 20);
                            llImg1.addView(imgViewOrigin);

                            TextView tvOrigin = new TextView(getActivity().getApplicationContext());
                            tvOrigin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0, 1));
                            tvOrigin.setGravity(Gravity.CENTER);
                            tvOrigin.setText(oriString);
                            tvOrigin.setTextSize(18);
                            tvOrigin.setTextColor(Color.BLACK);

                            TextView tvOriginHour = new TextView(getActivity().getApplicationContext());
                            tvOriginHour.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0, 1));
                            tvOriginHour.setGravity(Gravity.CENTER);
                            tvOriginHour.setText(s.getStopTime());
                            tvOriginHour.setTextSize(18);
                            tvOriginHour.setTextColor(Color.BLACK);

                            llImgViewOrigin.addView(llImg1);
                            llImgViewOrigin.addView(tvOrigin);
                            llImgViewOrigin.addView(tvOriginHour);

                            ///// LINEAR LAYOUT 2 / 3

                            LinearLayout llImgViewCenter = new LinearLayout(getActivity().getApplicationContext());
                            llImgViewCenter.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 2));
                            llImgViewCenter.setOrientation(LinearLayout.VERTICAL);
                            llImgViewCenter.setGravity(Gravity.CENTER);


                            LinearLayout llImgArrow = new LinearLayout(getActivity().getApplicationContext());
                            llImgArrow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 3));
                            llImgArrow.setGravity(Gravity.CENTER);
                            llImgArrow.setPadding(30, 30, 30, 30);

                            ImageView imgViewArrow = new ImageView(getActivity().getApplicationContext());
                            imgViewArrow.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            imgViewArrow.setBackground(getContext().getDrawable(R.drawable.arrows));
                            imgViewArrow.setAdjustViewBounds(true);
                            imgViewArrow.setScaleType(ImageView.ScaleType.FIT_CENTER);

                            llImgArrow.addView(imgViewArrow);

                            TextView price = new TextView(getActivity().getApplicationContext());
                            price.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 2));
                            price.setGravity(Gravity.CENTER);
                            price.setText(bd1.toString()+"€");
                            price.setTextSize(26);
                            price.setTextColor(Color.BLACK);

                            llImgViewCenter.addView(llImgArrow);
                            llImgViewCenter.addView(price);


                            //LINEAR LAYOUT 3 / 3
                            LinearLayout llImgViewDestination = new LinearLayout(getActivity().getApplicationContext());
                            llImgViewDestination.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 3));
                            llImgViewDestination.setOrientation(LinearLayout.VERTICAL);
                            llImgViewDestination.setGravity(Gravity.CENTER);

                            LinearLayout llImg2 = new LinearLayout(getActivity().getApplicationContext());
                            llImg2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 3));
                            llImg2.setGravity(Gravity.CENTER);

                            ImageView imgViewDestination = new ImageView(getActivity().getApplicationContext());
                            imgViewDestination.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            imgViewDestination.setBackground(getContext().getDrawable(R.drawable.ic_destination));
                            imgViewDestination.setScaleType(ImageView.ScaleType.CENTER);

                            llImg2.addView(imgViewDestination);

                            TextView tvDestination = new TextView(getActivity().getApplicationContext());
                            tvDestination.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0, 1));
                            tvDestination.setGravity(Gravity.CENTER);
                            tvDestination.setText(destString);
                            tvDestination.setTextSize(18);
                            tvDestination.setTextColor(Color.BLACK);

                            TextView tvDestinationHour = new TextView(getActivity().getApplicationContext());
                            tvDestinationHour.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0, 1));
                            tvDestinationHour.setGravity(Gravity.CENTER);
                            tvDestinationHour.setText(finalStop.getStopTime());
                            tvDestinationHour.setTextSize(18);
                            tvDestinationHour.setTextColor(Color.BLACK);

                            llImgViewDestination.addView(llImg2);
                            llImgViewDestination.addView(tvDestination);
                            llImgViewDestination.addView(tvDestinationHour);

                            //FINISH

                            tr.addView(llImgViewOrigin);
                            tr.addView(llImgViewCenter);
                            tr.addView(llImgViewDestination);
                            tripsTableLayout.addView(tr);//, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                        }


                    }
                }
            }
        });

        backToSelectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                travelDatePicker.setVisibility(View.VISIBLE);
                oriRL.setVisibility(View.VISIBLE);
                destRL.setVisibility(View.VISIBLE);
                seeTicketsButton.setVisibility(View.VISIBLE);
                dateSelectedTextView.setVisibility(View.GONE);
                backToSelectionButton.setVisibility(View.GONE);

                tripsTableLayout.setVisibility(View.GONE);
                tripsTableLayout.removeAllViews();
                tripsTableLayout.addView(topTableRow);
            }
        });


        travelDatePicker.setMinDate(System.currentTimeMillis()-1000);


        ((MainActivity)getActivity()).setFragmentRefreshListener(new MainActivity.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                updateAdapters();
            }
        });


        return root;
    }


    public void updateAdapters(){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.spinner_item){
            @Override
            public int getCount() {
                return super.getCount()-2;
            }
        };

        List<Station> stations = myApp.getStations();

        for (Station s: stations) {
            adapter.add(s.getName());
        }

        adapter.add("Origin Station");
        adapter.add("Destination Station");
        adapter.setDropDownViewResource(R.layout.spinner_item);

        destStation.setAdapter(adapter);
        destStation.setSelection(adapter.getCount()+1);
        oriStation.setAdapter(adapter);
        oriStation.setSelection(adapter.getCount());
    }



}