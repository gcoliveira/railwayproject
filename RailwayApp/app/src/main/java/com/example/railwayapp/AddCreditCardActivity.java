package com.example.railwayapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import java.util.Calendar;

public class AddCreditCardActivity extends AppCompatActivity {

    EditText cardNumberEditText, cardYearEditText, cardMonthEditText;
    LinearLayout cardLL;
    Button addCardButton;
    Socket ioSocket;
    RadioGroup cardTypeRadioGroup;
    RadioButton maestroRadioButton, visaRadioButton, masterCardRadioButton;
    MyApplication myApp;
    TextView errorTextViewAddCard;


    String cardNumber, cardType, cardValidity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        Intent intent = getIntent();

        myApp = (MyApplication) getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();


        cardLL = findViewById(R.id.cardLinearLayoutCardActivity);
        cardNumberEditText = findViewById(R.id.cardNumberEditBoxCardActivity);
        cardMonthEditText = findViewById(R.id.cardMonthEditTextCardActivity);
        cardYearEditText = findViewById(R.id.cardYearEditTextCardActivity);
        cardTypeRadioGroup = findViewById(R.id.cardTypeRadioGroupCardActivity);

        addCardButton = findViewById(R.id.addCardButton);
        masterCardRadioButton = findViewById(R.id.masterCardRadioButtonCardActivity);
        visaRadioButton = findViewById(R.id.visaRadioButtonCardActivity);
        maestroRadioButton = findViewById(R.id.maestroRadioButtonCardActivity);
        errorTextViewAddCard = findViewById(R.id.errorTextViewAddCard);

        addCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardNumber = cardNumberEditText.getText().toString();
                if (cardTypeRadioGroup.getCheckedRadioButtonId() == -1)
                {
                    maestroRadioButton.setError("Select a Card Type");
                    return;
                }
                RadioButton auxRB = findViewById(cardTypeRadioGroup.getCheckedRadioButtonId());//cardTypeEditText.getText().toString();
                cardType = auxRB.getText().toString();

                String valMonth = null, valYear = null;

                valMonth = cardMonthEditText.getText().toString();
                valYear = cardYearEditText.getText().toString();

                if(valMonth == null || valMonth.isEmpty() || Integer.parseInt(valMonth) > 12 || Integer.parseInt(valMonth) <= 0){
                    cardMonthEditText.setError("Invalid");
                    return;
                }

                int lastTwoDigits = Calendar.getInstance().get(Calendar.YEAR) % 100;
                if(valYear == null || valYear.isEmpty() || Integer.parseInt(valYear) < lastTwoDigits){
                    cardYearEditText.setError("Invalid");
                    return;
                }else if(Integer.parseInt(valYear) == lastTwoDigits &&
                        Integer.parseInt(valMonth) <= Calendar.getInstance().get(Calendar.MONTH)){
                    cardMonthEditText.setError("Invalid");
                    return;
                }

                cardValidity =  valMonth + "/" + valYear;


                if (cardNumber == null || cardNumber.isEmpty() || cardNumber.length() < 10) {
                    cardNumberEditText.setError("Card number not valid");
                    return;
                }

                if (cardType == null || cardType.isEmpty()) {
                    //cardTypeEditText.setError("Card type not valid");
                    return;
                }
                if (cardValidity == null || cardValidity.isEmpty()) {
                    cardMonthEditText.setError("Card Validity not valid");
                    cardYearEditText.setError("Card Validity not valid");
                    return;
                }
                //GenerateKeyPair(name);

                cardTypeRadioGroup.setEnabled(false);
                maestroRadioButton.setEnabled(false);
                visaRadioButton.setEnabled(false);
                masterCardRadioButton.setEnabled(false);

                cardMonthEditText.setEnabled(false);
                cardYearEditText.setEnabled(false);

                cardNumberEditText.setEnabled(false);
                addCardButton.setEnabled(false);

                errorTextViewAddCard.setVisibility(View.GONE);
                ioSocket.emit("addNewCard", cardNumber, cardType, cardValidity, myApp.getUser().getUuid());
            }
        });


    }

    private void setListening() {

        ioSocket.off("addNewCardSuccess");
        ioSocket.off("addNewCardError");


        ioSocket.on("addNewCardSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                myApp.addCreditCard(new Card((String)args[0], (String)args[1], (String)args[2]));
                Intent addedNewCard = new Intent();
                setResult(RESULT_OK, addedNewCard);
                finish();
            }
        });

        ioSocket.on("addNewCardError", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("GOT SOME ARGUMENTS IN ticket purchase error:" + args[0] + "; ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cardTypeRadioGroup.setEnabled(true);
                        maestroRadioButton.setEnabled(true);
                        visaRadioButton.setEnabled(true);
                        masterCardRadioButton.setEnabled(true);

                        cardMonthEditText.setEnabled(true);
                        cardYearEditText.setEnabled(true);

                        cardNumberEditText.setEnabled(true);
                        addCardButton.setEnabled(true);
                        errorTextViewAddCard.setText("Error adding the card");
                        errorTextViewAddCard.setVisibility(View.VISIBLE);
                    }
                });
            }
        });


    }
}
