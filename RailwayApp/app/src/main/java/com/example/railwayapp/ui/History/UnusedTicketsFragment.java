package com.example.railwayapp.ui.History;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.railwayapp.MyApplication;
import com.example.railwayapp.R;
import com.example.railwayapp.Station;
import com.example.railwayapp.Ticket;
import com.example.railwayapp.User;
import com.example.railwayapp.ui.TicketActivity;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UnusedTicketsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private MyApplication myApp;
    private View myFragment;
    private ListView unusedTicketsList;
    private ArrayAdapter<Ticket> adapter;
    RelativeLayout loadingPanelUnusedTickets;
    List<Ticket> unusedTicketsDataProvider = new ArrayList<>();
    private Socket ioSocket;

    public UnusedTicketsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myApp = (MyApplication) getActivity().getApplicationContext();
        ioSocket = myApp.getSocket();

        setListening();

        myFragment = inflater.inflate(R.layout.fragment_unused_tickets, container, false);

        //JUST FOR TESTING
        //addMockedData();
        //JUST FOR TESTING


        myApp.getSocket().emit("getUnusedTickets", myApp.getUser().getUuid());

        loadingPanelUnusedTickets = myFragment.findViewById(R.id.loadingPanelUnusedTickets);

        unusedTicketsList = myFragment.findViewById(R.id.unusedTicketsList);


        // Inflate the layout for this fragment
        return myFragment;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
        Intent intent = new Intent(getActivity().getApplicationContext(), TicketActivity.class);
        intent.putExtra("SELECTED_TICKET", unusedTicketsDataProvider.get(pos).getUuid());
        startActivity(intent);
    }

    public void testOnClick() {
        unusedTicketsList.setOnItemClickListener(this);
    }

    class UnusedTicketsAdapter extends ArrayAdapter<Ticket> {

        UnusedTicketsAdapter() {
            super(getActivity().getApplicationContext(), R.layout.unused_tickets_row, unusedTicketsDataProvider);
        }

        @Override
        public @NonNull
        View getView(int position, View convertView, @NonNull ViewGroup parent) {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-YYYY");
            View row = convertView;

            if (row == null) {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(R.layout.unused_tickets_row, parent, false);
            }

            Ticket t = unusedTicketsDataProvider.get(position);

            ((TextView) row.findViewById(R.id.title)).setText(t.getOrigin().getName() + " > " + t.getDestination().getName());
            ((TextView) row.findViewById(R.id.date)).setText(t.getTravelDate() + " - " + t.getTravelTime());

            return (row);
        }

    }

    private void setListening() {

        ioSocket.off("noUnusedTickets");
        ioSocket.off("gotUnusedTickets");

        ioSocket.on("noUnusedTickets", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingPanelUnusedTickets.setVisibility(View.GONE);
                        unusedTicketsList.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

        ioSocket.on("gotUnusedTickets", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingPanelUnusedTickets.setVisibility(View.GONE);
                        unusedTicketsList.setVisibility(View.VISIBLE);
                        try {
                            JSONArray unusedTickets = new JSONArray(args[0].toString());
                            for (int i = 0; i < unusedTickets.length(); i++) {
                                JSONObject tempTicket = unusedTickets.getJSONObject(i);
                                unusedTicketsDataProvider.add(new Ticket(tempTicket.getString("ticketUuid"),
                                        myApp.getStationByNumber(tempTicket.getInt("initialStationNumber")),
                                        myApp.getStationByNumber(tempTicket.getInt("finalStationNumber")),
                                        tempTicket.getString("date"),
                                        false,
                                        tempTicket.getString("time")));
                                /*Log.println(Log.ASSERT, "unused tickts received:", "Name: " + tempTicket.getString("ticketUuid") + ";\n" +
                                        myApp.getStationByNumber(tempTicket.getInt("initialStationNumber")) + " -> " +
                                        myApp.getStationByNumber(tempTicket.getInt("finalStationNumber")) + ";\n" +
                                        tempTicket.getString("ticketUuid"));*/
                            }
                            Collections.sort(unusedTicketsDataProvider);
                            myApp.getUser().setUnusedTickets(unusedTicketsDataProvider);
                        } catch (Exception e) {

                        }

                        //usedTicketsDataProvider = myApp.getUser().getUserUsedTickets(); //HERE

                        //unusedTicketsDataProvider = myApp.getUser().getUnusedTickets();

                        adapter = new UnusedTicketsAdapter();
                        unusedTicketsList.setAdapter(adapter);
                        testOnClick();


                    }
                });
            }
        });
    }
}