package com.example.railwayapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.snackbar.Snackbar;


import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.xml.datatype.Duration;

public class LoginActivity extends AppCompatActivity {

    EditText nameEditText, passwordEditText;
    TextView errorTextView;
    Button chooseLoginButton, loginButton, registerButton;
    Socket ioSocket;
    String name, password, uuid;
    String lastUsedName = null;
    private SharedPreferences sharedPref;
    User tempUser;
    RelativeLayout loadingPanelLoginActivity;

    MyApplication myApp;

    boolean loginSelect = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myApp = (MyApplication) getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();

        sharedPref = getApplicationContext().getSharedPreferences("railwaySP", Context.MODE_PRIVATE);
        uuid = sharedPref.getString("uuid", null);
        Map<String, ?> allEntries = sharedPref.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.println(Log.ASSERT, "map values", entry.getKey() + ": " + entry.getValue().toString());
        }
        if(uuid != null){
            ioSocket.emit("alreadyLoggedIn", uuid, new Ack() {
                @Override
                public void call(Object... args) {
                    String res = (String) args[args.length - 1];
                    if(!res.equals("No result")){
                        lastUsedName = res;
                        System.out.println("Got a login with username: "+ lastUsedName);
                    }
                }
            });
        }
        getSupportActionBar().setTitle("Login");


        nameEditText = findViewById(R.id.nameEditText);
        nameEditText.setVisibility(View.GONE);
        passwordEditText = findViewById(R.id.passwordEditText);
        passwordEditText.setVisibility(View.GONE);

        errorTextView = findViewById(R.id.errorTextView);
        errorTextView.setVisibility(View.GONE);

        chooseLoginButton = findViewById(R.id.chooseLoginButton);
        loginButton = findViewById(R.id.loginButton);
        loginButton.setVisibility(View.GONE);
        registerButton = findViewById(R.id.registerButton);

        loadingPanelLoginActivity = findViewById(R.id.loadingPanelLoginActivity);



        chooseLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorTextView.setVisibility(View.GONE);
                //REMOVE THE FALSE FROM THIS IF TO SEE IF THERE ARE A LOGIN AVAILABLE
                if(uuid != null && lastUsedName != null && ioSocket.connected()) {// && false) {
                    Log.println(Log.ASSERT, "Login WSA", "Got a previous login, trying to login...");
                    if(myApp.getKeyManager().storeKeysFromAndroidKeyStoreInKeyManager(lastUsedName)) {
                        loadingPanelLoginActivity.setVisibility(View.VISIBLE);
                        name = lastUsedName;
                        String pubKeyAsPEM = (myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getPubKey()));
                        ioSocket.emit("loginWithSameAccount", lastUsedName, pubKeyAsPEM);
                    }else{
                        loginFailedWithSameAccount("No keys for quick login");
                        Log.println(Log.ASSERT, "Login WSA failed", "There are no keys for this account in the AndroidKeyStore, re-login!");
                        showLoginForm();
                    }
                }else {
                    showLoginForm();
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorTextView.setVisibility(View.GONE);
                if(!ioSocket.connected()){
                    errorTextView.setText("Not Connected to server");
                    errorTextView.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), "Not connected to server", Toast.LENGTH_SHORT);
                    return;
                }
                name = nameEditText.getText().toString();
                password = passwordEditText.getText().toString();

                if(name.isEmpty() || name == null){
                    nameEditText.setError("Invalid name");
                }else if(password.isEmpty() || password == null){
                    passwordEditText.setError("Invalid password");
                }else{

                    loadingPanelLoginActivity.setVisibility(View.VISIBLE);



                    nameEditText.setEnabled(false);
                    passwordEditText.setEnabled(false);
                    loginButton.setEnabled(false);

                    if(myApp.getKeyManager().storeKeysFromAndroidKeyStoreInKeyManager(name)){
                        String pubKeyAsPEM = myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getPubKey());
                        ioSocket.emit("loginAndCompareKeys", name, password, pubKeyAsPEM);
                    }else{
                        myApp.getKeyManager().GenerateKeyPair(getApplicationContext(), name);
                        String pubKeyAsPEM = myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getPubKey());
                        ioSocket.emit("loginAndUpdateKeys", name, password, pubKeyAsPEM);
                    }
                }
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterNewUser();
            }
        });
    }

    private void showLoginForm(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loginSelect = true;
                loadingPanelLoginActivity.setVisibility(View.GONE);
                chooseLoginButton.setVisibility(View.GONE);
                registerButton.setVisibility(View.GONE);
                loginButton.setVisibility(View.VISIBLE);
                nameEditText.setVisibility(View.VISIBLE);
                passwordEditText.setVisibility(View.VISIBLE);

            }
        });
    }

    private void RegisterNewUser() {
        Intent registerData = new Intent(this, RegisterActivity.class);
        startActivityForResult(registerData, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                System.out.println("GOT BACK FROM REGISTER");
                ioSocket.emit("prepareToGetInitialDataFromDB", myApp.getUser().getUuid());

                Intent backToHomePage = new Intent();
                setResult(RESULT_OK, backToHomePage);
                finish();
            }
        }
    }

    private void LoginSuccess(Object... args) {
        String challenge = (String)args[5];
        //Log.println(Log.ASSERT, "challenge:", challenge);
        tempUser = new User((String)args[0], name, (String)args[1], password, (String)args[2], (String)args[3]);
        myApp.getKeyManager().setServerPubKey(myApp.getKeyManager().GeneratePublicKeyFromPEM((String)args[4]));

        String encryptedChallenge = myApp.getKeyManager().encryptChallenge(UUID.fromString(challenge));
        ioSocket.emit("sendEncryptedChallengeToServer", encryptedChallenge , tempUser.getName(),  new Ack() {
            @Override
            public void call(Object... args) {
                if(args[0].equals("false")){
                    //Log.println(Log.ASSERT, "Challenge return", "Challenges don't match");
                    if(loginSelect){
                        loginFailed("Challenges don't match");
                    }else{
                        loginFailedWithSameAccount("Challenges don't match");
                    }
                }else{
                    //Log.println(Log.ASSERT, "Challenge return", "Challenges match");
                    LoginFinished();
                }
            }
        });



    }

    private void LoginFinished() {
        MyApplication myApp = (MyApplication) getApplicationContext();
        myApp.setUser(tempUser);
        System.out.println(myApp.getUser().toString());

        //MAIS A FRENTE IR BUSCAR OS CARTOES E DADOS DE VIAGENS AO FAZER LOGIN

        if(uuid==null || !uuid.equals(tempUser.getUuid())) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("uuid", tempUser.getUuid());
            editor.commit();
        }

        ioSocket.emit("prepareToGetInitialDataFromDB", tempUser.getUuid());

        Intent backToHomePage = new Intent();
        setResult(RESULT_OK, backToHomePage);
        finish();
    }

    @Override
    public void onBackPressed() {
        if(loginSelect){
            loginSelect = false;
            errorTextView.setVisibility(View.GONE);
            chooseLoginButton.setVisibility(View.VISIBLE);
            registerButton.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.GONE);
            nameEditText.setVisibility(View.GONE);
            passwordEditText.setVisibility(View.GONE);
        }else
            Toast.makeText(this, "You need to Login!", Toast.LENGTH_SHORT);
    }

    private void loginFailedWithSameAccount(String err) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                lastUsedName = null;
                uuid = null;
                name = null;
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove(("uuid"));
                editor.commit();
                loadingPanelLoginActivity.setVisibility(View.GONE);
                //loginFailed(err);
            }
        });
    }

    private void loginFailed(final String err){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                errorTextView.setText(err);
                errorTextView.setVisibility(View.VISIBLE);

                loadingPanelLoginActivity.setVisibility(View.GONE);
                nameEditText.setEnabled(true);
                passwordEditText.setEnabled(true);
                loginButton.setEnabled(true);
            }
        });

    }

    private void setListening(){


        ioSocket.off("loginFailed");
        ioSocket.off("loginSuccess");
        ioSocket.off("loginSuccessWithSameAccount");
        ioSocket.off("loginFailedWithSameAccount");

        ioSocket.on("loginSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("LOGIN WAS A SUCCESS");
                LoginSuccess(args);
            }
        });

        ioSocket.on("loginSuccessWithSameAccount", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("LOGIN WITH SAME ACCOUNT WAS A SUCCESS");
                LoginSuccess(args);
            }
        });

        ioSocket.on("loginFailedWithSameAccount", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                loginFailedWithSameAccount((String)args[0]);
                Log.println(Log.ERROR, "Login WSAccount failed", (String)args[0]);
                showLoginForm();
            }
        });

        ioSocket.on("loginFailed", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                loginFailed((String)args[0]);
            }
        });

    }
}
