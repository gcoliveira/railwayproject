package com.example.railwayapp.ui.Profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.railwayapp.AddCreditCardActivity;
import com.example.railwayapp.Card;
import com.example.railwayapp.MainActivity;
import com.example.railwayapp.MyApplication;
import com.example.railwayapp.R;
import com.example.railwayapp.Ticket;
import com.example.railwayapp.User;
import com.example.railwayapp.ui.History.UsedTicketsFragment;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    private TableLayout creditCardTableLayoutProfile;
    private EditText birthdayEditTextProfile, emailEditTextProfile, nameEditTextProfile, nationalityEditTextProfile;
    private Button newCardButtonProfile, logoutButton;
    private Socket ioSocket;
    private SharedPreferences sharedPref;

    private MyApplication myApp;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        myApp = (MyApplication) getActivity().getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();

        birthdayEditTextProfile = root.findViewById(R.id.birthdayEditTextProfile);
        emailEditTextProfile = root.findViewById(R.id.emailEditTextProfile);
        nameEditTextProfile = root.findViewById(R.id.nameEditTextProfile);
        nationalityEditTextProfile = root.findViewById(R.id.nationalityEditTextProfile);


        sharedPref = getActivity().getApplicationContext().getSharedPreferences("railwaySP", Context.MODE_PRIVATE);


        if(myApp.getUser() != null){
            birthdayEditTextProfile.setText(myApp.getUser().getBirthdate());
            emailEditTextProfile.setText(myApp.getUser().getEmail());
            nameEditTextProfile.setText(myApp.getUser().getName());
            nationalityEditTextProfile.setText(myApp.getUser().getNationality());
        }


        creditCardTableLayoutProfile = root.findViewById(R.id.creditCardTableLayoutProfile);
        updateCreditcards();

        newCardButtonProfile = root.findViewById(R.id.newCardButtonProfile);

        newCardButtonProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newCard = new Intent(getActivity().getApplicationContext(), AddCreditCardActivity.class);
                startActivityForResult(newCard, 1);
            }
        });

        logoutButton = root.findViewById(R.id.logoutButton);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myApp.setUser(null);
                myApp.setLoggedin(false);
                myApp.setCreditCards(new ArrayList<>());
                getActivity().recreate();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove(("uuid"));
                editor.commit();
            }
        });

        ((MainActivity)getActivity()).setFragmentRefreshListener(new MainActivity.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPage();
            }
        });

        return root;
    }

    private void refreshPage() {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                birthdayEditTextProfile.setText(myApp.getUser().getBirthdate());
                emailEditTextProfile.setText(myApp.getUser().getEmail());
                nameEditTextProfile.setText(myApp.getUser().getName());
                nationalityEditTextProfile.setText(myApp.getUser().getNationality());

                updateCreditcards();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        updateCreditcards();
    }

    public void updateCreditcards(){

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                creditCardTableLayoutProfile.removeAllViews();

                List<Card> creditCards = myApp.getCreditCards();

                for(Card c : creditCards) {
                    Log.println(Log.ASSERT, "Credit cards: ", c.getNumber()+" - "+c.getType());
                    //IMAGE VIEW
                    ImageView imgCreditCard = new ImageView(getActivity().getApplicationContext());
                    TableRow.LayoutParams trlp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
                    trlp.setMargins(20, 20, 20, 20);
                    imgCreditCard.setLayoutParams(trlp);
                    imgCreditCard.setBackground(getActivity().getDrawable(R.drawable.ic_credit_card));


                    LinearLayout mainLinearLayout = new LinearLayout(getActivity().getApplicationContext());
                    mainLinearLayout.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1));
                    mainLinearLayout.setGravity(Gravity.CENTER);
                    mainLinearLayout.setOrientation(LinearLayout.VERTICAL);

                    LinearLayout firstLinearLayout = new LinearLayout(getActivity().getApplicationContext());
                    firstLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                    //TEXT VIEW 1-1
                    TextView tvCardType = new TextView(getActivity().getApplicationContext());
                    tvCardType.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 2));
                    tvCardType.setGravity(Gravity.LEFT);
                    tvCardType.setText(c.getType());
                    //TEXT VIEW 1-2
                    TextView tvCardVal = new TextView(getActivity().getApplicationContext());
                    tvCardVal.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1));
                    tvCardVal.setGravity(Gravity.LEFT);
                    tvCardVal.setText(c.getValidity());

                    firstLinearLayout.addView(tvCardType);
                    firstLinearLayout.addView(tvCardVal);

                    //TEXT VIEW 2
                    TextView tvCardNum = new TextView(getActivity().getApplicationContext());
                    tvCardNum.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    tvCardNum.setGravity(Gravity.LEFT);
                    tvCardNum.setText(c.getNumber());
                    tvCardNum.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

                    mainLinearLayout.addView(firstLinearLayout);
                    mainLinearLayout.addView(tvCardNum);


                    ImageView imgDeleteCard = new ImageView(getActivity().getApplicationContext());
                    imgDeleteCard.setLayoutParams(trlp);
                    imgDeleteCard.setBackground(getActivity().getDrawable(android.R.drawable.ic_delete));
                    imgDeleteCard.setTag(c);
                    imgDeleteCard.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            v.setEnabled(false);
                            Card c = (Card) v.getTag();
                            String cardNumber = c.getNumber();
                            myApp.removeCreditCard(c);
                            myApp.getSocket().emit("removeCreditCard", cardNumber);
                        }
                    });


                    TableRow tr = new TableRow(getActivity().getApplicationContext());
                    tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
                    tr.setGravity(Gravity.CENTER);
                    tr.setBackground(getContext().getDrawable(R.drawable.table_normalcell_border));

                    tr.addView(imgCreditCard); //add image
                    tr.addView(mainLinearLayout); //add ll
                    tr.addView(imgDeleteCard);

                    creditCardTableLayoutProfile.addView(tr);
                }
            }
        });

    }

    private void setListening() {

        ioSocket.off("removeCreditCardSuccess");

        ioSocket.on("removeCreditCardSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                updateCreditcards();
            }
        });

    }
}