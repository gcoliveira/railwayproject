package com.example.railwayapp.ui.Timetable;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.railwayapp.R;

import java.util.ArrayList;
import java.util.List;

import com.example.railwayapp.MyApplication;
import com.example.railwayapp.Station;
import com.example.railwayapp.Stop;
import com.example.railwayapp.Trip;

public class TimetableFragment extends Fragment  {
    private MyApplication myApp;
    //TESTE
    private ListView stationsList;
    private ArrayAdapter<Station> adapter_stations;
    List<Station> stationsDataProvider = new ArrayList<>();

    private ListView stopsList;
    private ArrayAdapter<Stop> adapter_stops;
    List<Stop> stopsDataProvider = new ArrayList<>();
    private TableLayout timetableTableLayout;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        myApp = (MyApplication) getActivity().getApplicationContext();

        final View root = inflater.inflate(R.layout.fragment_timetable, container, false);

        timetableTableLayout = root.findViewById(R.id.timetableTableLayout);


        //PADEIRO
        List<Station> allStations = new ArrayList<>();
        allStations=myApp.getStations();
        List<Stop> allStops = new ArrayList<>();
        allStops=myApp.getStops();

        boolean switchit = false;

        for(int i = 1; i < allStops.size(); i=i+5){




            TableRow tr = new TableRow(getActivity().getApplicationContext());
            tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1));
            tr.setBackground(getActivity().getDrawable(R.drawable.table_normalcell_border));

            ImageView imgArrow = new ImageView(getActivity().getApplicationContext());
            LinearLayout.LayoutParams lllpiv = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lllpiv.setMargins(5, 5, 5, 5);
            imgArrow.setLayoutParams(lllpiv);
            if(!switchit)
                imgArrow.setBackground(getActivity().getDrawable(R.drawable.ic_timetable_arrow_right));
            else
                imgArrow.setBackground(getActivity().getDrawable(R.drawable.ic_timetable_arrow_left));


            LinearLayout imgLL = new LinearLayout(getActivity().getApplicationContext());
            imgLL.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 2));
            imgLL.addView(imgArrow);


            TextView tv1 = new TextView(getActivity().getApplicationContext());
            tv1.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 3));
            tv1.setGravity(Gravity.CENTER);
            tv1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tv1.setText(myApp.getStopById(i).getStopTime());

            TextView tv2 = new TextView(getActivity().getApplicationContext());
            tv2.setLayoutParams(new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 3));
            tv2.setGravity(Gravity.CENTER);
            tv2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tv2.setText(myApp.getStopById(i+1).getStopTime());

            TextView tv3 = new TextView(getActivity().getApplicationContext());
            tv3.setLayoutParams(new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 3));
            tv3.setGravity(Gravity.CENTER);
            tv3.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tv3.setText(myApp.getStopById(i+2).getStopTime());

            TextView tv4 = new TextView(getActivity().getApplicationContext());
            tv4.setLayoutParams(new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 3));
            tv4.setGravity(Gravity.CENTER);
            tv4.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tv4.setText(myApp.getStopById(i+3).getStopTime());

            TextView tv5 = new TextView(getActivity().getApplicationContext());
            tv5.setLayoutParams(new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 3));
            tv5.setGravity(Gravity.CENTER);
            tv5.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
            tv5.setText(myApp.getStopById(i+4).getStopTime());

            tr.addView(imgLL);
            if(!switchit) {
                tr.addView(tv1);
                tr.addView(tv2);
                tr.addView(tv3);
                tr.addView(tv4);
                tr.addView(tv5);
            }else{
                tr.addView(tv5);
                tr.addView(tv4);
                tr.addView(tv3);
                tr.addView(tv2);
                tr.addView(tv1);

            }
            if(switchit)
                switchit = false;
            else
                switchit=true;

            timetableTableLayout.addView(tr);


        }







        return root;
    }

}