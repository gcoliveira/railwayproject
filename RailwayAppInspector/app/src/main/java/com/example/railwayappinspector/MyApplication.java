package com.example.railwayappinspector;

import android.app.Application;
import android.util.Log;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class MyApplication extends Application {

    private KeyManager keyManager = new KeyManager();
    private Socket ioSocket;
    private Inspector currentInspector;
    private List<Stop> stops = new ArrayList<>();
    private List<Trip> trips = new ArrayList<>();
    private List<Station> stations = new ArrayList<>();
    private List<Confirmation> confirmations = new ArrayList<>();
    private boolean loggedin = false;
    private Trip selectedTrip = null;
    private String selectedTripDate = null;
    private int existing = 0, validated = 0;

    {
        try {
            ioSocket = IO.socket("http://192.168.1.17:3000");
        } catch (URISyntaxException e) {
            Log.d("myTag", e.getMessage());
        }
    }
    public void connect(){

        try {
            ioSocket.connect();
            Log.println(Log.ASSERT, "myTag", "TRY YO CONNECT");
        }catch(Exception e){
            Log.println(Log.ASSERT, "myTag", e.getMessage());

        }
    }

    public Socket getSocket(){
        return ioSocket;
    }

    public void setInspector(Inspector inspector){
        this.currentInspector = inspector;
    }
    public Inspector getInspector(){
        return this.currentInspector;
    }


    public Trip getSelectedTrip() {
        return selectedTrip;
    }

    public void setSelectedTrip(Trip selectedTrip) {
        this.selectedTrip = selectedTrip;
    }

    public String getSelectedTripDate() {
        return selectedTripDate;
    }

    public void setSelectedTripDate(String selectedTripDate) {
        this.selectedTripDate = selectedTripDate;
    }

    public Stop getStopById(int stopId){
        for(Stop s : this.stops){
            if(s.getStopId() == stopId)
                return s;
        }
        return null;
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public List<Stop> getStopsInStation (int stationNumber){

        List<Stop> tempStops = new ArrayList<>();

        for (Stop s: this.stops) {
            if(s.getStationNumber() == stationNumber){
                tempStops.add(s);
            }
        }

        return tempStops;
    }

    public List<Stop> getStopsInStationMovingUp (int stationNumber){

        List<Stop> tempStops = new ArrayList<>();

        for (Stop s:getStopsInStation(stationNumber)) {
            if(s.getNextStationNumber() > stationNumber){
                tempStops.add(s);
            }
        }

        return tempStops;
    }

    public List<Stop> getStopsInStationMovingDown (int stationNumber){

        List<Stop> tempStops = new ArrayList<>();

        for (Stop s:getStopsInStation(stationNumber)) {
            if(s.getNextStationNumber() < stationNumber && s.getNextStationNumber()!=0){
                tempStops.add(s);
            }
        }

        return tempStops;
    }

    public Trip getTripWithId(int id){
        for(Trip t : trips){
            if(t.getTripId() == id)
                return t;
        }
        return null;
    }

    public void addStop(Stop stop){
        this.stops.add(stop);
    }

    public void addStation(Station station){
        this.stations.add(station);
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public List<Station> getStations() {
        return stations;
    }

    public Station getStationByName(String stationName){

        for (Station s: this.stations) {
            if(s.getName().equalsIgnoreCase(stationName)){
                return s;
            }
        }
        return null;
    }

    public Station getStationByNumber(int stationNumber){

        for (Station s: this.stations) {
            if(stationNumber == s.getStationNumber()){
                return s;
            }
        }
        return null;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void addTrip(Trip trip){
        this.trips.add(trip);
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public boolean isLoggedin() {
        return loggedin;
    }

    public void setLoggedin(boolean loggedin) {
        this.loggedin = loggedin;
    }

    public void addConfirmation(Confirmation c){
        confirmations.add(c);
    }

    public void clearConfirmations(){
        confirmations.clear();
    }

    public List<Confirmation> getConfirmations() {
        return confirmations;
    }

    public void setConfirmations(List<Confirmation> confirmations) {
        this.confirmations = confirmations;
    }

    public int getExisting() {
        return existing;
    }

    public void setExisting(int existing) {
        this.existing = existing;
    }

    public int getValidated() {
        return validated;
    }

    public void setValidated(int validated) {
        this.validated = validated;
    }

    public void clearData() {
        this.stops = new ArrayList<>();
        this.trips = new ArrayList<>();
        this.stations = new ArrayList<>();
    }
}