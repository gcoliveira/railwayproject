package com.example.railwayappinspector.ui.nfc;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.railwayappinspector.R;

public class NFCFragment extends Fragment {

    private NFCViewModel NFCViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        NFCViewModel =
                ViewModelProviders.of(this).get(NFCViewModel.class);
        View root = inflater.inflate(R.layout.fragment_nfc, container, false);

        return root;
    }
}
