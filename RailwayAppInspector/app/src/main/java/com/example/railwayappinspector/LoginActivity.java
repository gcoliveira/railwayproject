package com.example.railwayappinspector;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.Socket;

import java.util.Map;
import java.util.UUID;

public class LoginActivity extends AppCompatActivity {

    EditText nameEditText, passwordEditText;
    TextView errorTextView;
    Button chooseLoginButton, loginButton;
    Socket ioSocket;
    String name, password, uuid;
    String lastUsedName = null;
    private SharedPreferences sharedPref;
    Inspector tempInspector;
    RelativeLayout loadingPanelLoginActivity;

    MyApplication myApp;

    boolean loginSelect = false, tryingLogin = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myApp = (MyApplication) getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();

        sharedPref = getApplicationContext().getSharedPreferences("railwaySP", Context.MODE_PRIVATE);
        uuid = sharedPref.getString("uuid", null);
        Map<String, ?> allEntries = sharedPref.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.println(Log.ASSERT, "map values", entry.getKey() + ": " + entry.getValue().toString());
        }
        if(uuid != null){
            ioSocket.emit("alreadyLoggedInInspector", uuid, new Ack() {
                @Override
                public void call(Object... args) {
                    String res = (String) args[args.length - 1];
                    if(!res.equals("No result")){
                        lastUsedName = res;
                        System.out.println("Got a login with username: "+ lastUsedName);
                    }
                }
            });
        }
        getSupportActionBar().setTitle("Login");


        nameEditText = findViewById(R.id.nameEditText);
        nameEditText.setVisibility(View.GONE);
        passwordEditText = findViewById(R.id.passwordEditText);
        passwordEditText.setVisibility(View.GONE);

        errorTextView = findViewById(R.id.errorTextView);
        errorTextView.setVisibility(View.GONE);

        chooseLoginButton = findViewById(R.id.chooseLoginButton);
        loginButton = findViewById(R.id.loginButton);
        loginButton.setVisibility(View.GONE);

        loadingPanelLoginActivity = findViewById(R.id.loadingPanelLoginActivity);



        chooseLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorTextView.setVisibility(View.GONE);
                //REMOVE THE FALSE FROM THIS IF TO SEE IF THERE ARE A LOGIN AVAILABLE
                if(uuid != null && lastUsedName != null && ioSocket.connected()) {// && false) {
                    Log.println(Log.ASSERT, "Login WSA", "Got a previous login, trying to login...");
                    loadingPanelLoginActivity.setVisibility(View.VISIBLE);
                    name = lastUsedName;
                    ioSocket.emit("loginWithSameAccountInspector", lastUsedName);
                }else {
                    showLoginForm();
                }
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorTextView.setVisibility(View.GONE);
                if(!ioSocket.connected()){
                    errorTextView.setText("Not Connected to server");
                    errorTextView.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), "Not connected to server", Toast.LENGTH_SHORT);
                    return;
                }
                name = nameEditText.getText().toString();
                password = passwordEditText.getText().toString();

                if(name.isEmpty() || name == null){
                    nameEditText.setError("Invalid name");
                }else if(password.isEmpty() || password == null){
                    passwordEditText.setError("Invalid password");
                }else{

                    loadingPanelLoginActivity.setVisibility(View.VISIBLE);



                    tryingLogin = true;
                    nameEditText.setEnabled(false);
                    passwordEditText.setEnabled(false);
                    loginButton.setEnabled(false);

                    ioSocket.emit("loginAsInspector", name, password);
                }
            }
        });

    }

    private void showLoginForm(){
        loginSelect = true;
        loadingPanelLoginActivity.setVisibility(View.GONE);
        chooseLoginButton.setVisibility(View.GONE);
        loginButton.setVisibility(View.VISIBLE);
        nameEditText.setVisibility(View.VISIBLE);
        passwordEditText.setVisibility(View.VISIBLE);
    }

    private void LoginSuccess(String name, String insUuid) {
        MyApplication myApp = (MyApplication) getApplicationContext();
        myApp.setInspector(new Inspector(insUuid, name));
        System.out.println(myApp.getInspector().toString());

        tryingLogin = false;

        //MAIS A FRENTE IR BUSCAR OS CARTOES E DADOS DE VIAGENS AO FAZER LOGIN

        if(uuid==null || !uuid.equals(insUuid)) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("uuid", insUuid);
            editor.commit();
        }

        ioSocket.emit("prepareToGetInitialDataFromDB", insUuid);

        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        Intent backToHomePage = new Intent();
        setResult(RESULT_OK, backToHomePage);
        finish();
    }

    @Override
    public void onBackPressed() {
        if(tryingLogin)
            return;
        if(loginSelect){
            loginSelect = false;
            errorTextView.setVisibility(View.GONE);
            chooseLoginButton.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.GONE);
            nameEditText.setVisibility(View.GONE);
            passwordEditText.setVisibility(View.GONE);
        }else
            Toast.makeText(this, "You need to Login!", Toast.LENGTH_SHORT);
    }

    private void loginFailedWithSameAccount(String err) {
        lastUsedName = null;
        uuid = null;
        name = null;
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(("uuid"));
        editor.commit();
        loadingPanelLoginActivity.setVisibility(View.GONE);
        //loginFailed(err);
    }

    private void loginFailed(final String err){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                errorTextView.setText(err);
                errorTextView.setVisibility(View.VISIBLE);

                loadingPanelLoginActivity.setVisibility(View.GONE);
                nameEditText.setEnabled(true);
                passwordEditText.setEnabled(true);
                loginButton.setEnabled(true);
            }
        });

    }

    private void setListening(){


        ioSocket.off("loginFailed");
        ioSocket.off("loginSuccess");
        ioSocket.off("loginSuccessWithSameAccount");
        ioSocket.off("loginFailedWithSameAccount");

        ioSocket.on("loginSuccess", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                tryingLogin = false;
                System.out.println("LOGIN WAS A SUCCESS");
                myApp.getKeyManager().setServerPubKey(myApp.getKeyManager().GeneratePublicKeyFromPEM((String)args[2]));
                LoginSuccess((String)args[0], (String)args[1]);
            }
        });

        ioSocket.on("loginSuccessWithSameAccount", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                tryingLogin = false;
                System.out.println("LOGIN WITH SAME ACCOUNT WAS A SUCCESS");
                myApp.getKeyManager().setServerPubKey(myApp.getKeyManager().GeneratePublicKeyFromPEM((String)args[2]));
                LoginSuccess((String)args[0], (String)args[1]);
            }
        });

        ioSocket.on("loginFailedWithSameAccount", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                tryingLogin = false;
                loginFailedWithSameAccount((String)args[0]);
                Log.println(Log.ERROR, "Login WSAccount failed", (String)args[0]);
                showLoginForm();
            }
        });

        ioSocket.on("loginFailed", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                loginFailed((String)args[0]);
                tryingLogin = false;
            }
        });

    }
}
