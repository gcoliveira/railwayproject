package com.example.railwayappinspector;

public class Station {

    private int stationNumber;
    private String name;


    public Station(int stationNumber, String name){
        this.stationNumber = stationNumber;
        this.name = name;
    }

    public int getStationNumber() {
        return stationNumber;
    }

    public void setStationNumber(int stationNumber) {
        this.stationNumber = stationNumber;
    }

    public String getName() {
        return name;
    }

    public void getName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Station{" +
                "stationNumber=" + stationNumber +
                ", name='" + name + '\'' +
                '}';
    }
}
