package com.example.railwayappinspector;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.bouncycastle.util.io.pem.PemWriter;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.security.auth.x500.X500Principal;

public class KeyManager {

    private final String KEY_ALIAS = "RailwayKey_";
    static final String ENC_ALGO = "RSA/ECB/PKCS1Padding";   // encrypt/decrypt algorithm
    PublicKey pubKey;
    PrivateKey priKey;
    PublicKey serverPubKey;

    public boolean GenerateKeyPair(Context context, String username) {
        try {
            Calendar start = new GregorianCalendar();
            Calendar end = new GregorianCalendar();
            end.add(Calendar.YEAR, 20);

            KeyPairGenerator kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");
            kpg.initialize(new KeyPairGeneratorSpec.Builder(context)
                    .setKeySize(Constants.KEY_SIZE)
                    .setAlias(KEY_ALIAS+username)                                       // the name of the key (common name) to retrieve it
                    .setSubject(new X500Principal("CN=" + KEY_ALIAS+username))
                    .setSerialNumber(BigInteger.valueOf(Constants.CERT_SERIAL))       // a serial number to the public key certificate
                    .setStartDate(start.getTime())
                    .setEndDate(end.getTime())
                    .build());

            KeyPair kp = kpg.generateKeyPair();
            this.pubKey = kp.getPublic();
            this.priKey = kp.getPrivate();

            if (checkIfKeyExists(KEY_ALIAS + username)){
                System.out.println("The key with " + KEY_ALIAS + username + " exists");
                return true;
            }
            else {
                System.out.println("There is no key with alias: " + KEY_ALIAS + username);
                return false;
            }
        }catch ( NoSuchAlgorithmException |  InvalidAlgorithmParameterException | NoSuchProviderException e){
            Log.e("GenerateKeyPair", e.toString());
            return false;
        }

    }

    public boolean storeKeysFromAndroidKeyStoreInKeyManager(String username){
        try{
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            KeyStore.Entry entry = keyStore.getEntry(KEY_ALIAS+username, null);
            this.priKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
            this.pubKey = keyStore.getCertificate(KEY_ALIAS+username).getPublicKey();
            return true;
        }catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableEntryException | NullPointerException e){
            e.printStackTrace();
            return false;
        }
    }

    //Receives a public key and ceates a PKCS1 PEM string
    public String getPubKeyAsPEMString(PublicKey pub) {
        try {
            byte[] pubBytes = pub.getEncoded();

            SubjectPublicKeyInfo spkInfo = SubjectPublicKeyInfo.getInstance(pubBytes);
            ASN1Primitive primitive = spkInfo.parsePublicKey();
            byte[] publicKeyPKCS1 = primitive.getEncoded();

            PemObject pemObject = new PemObject("RSA PUBLIC KEY", publicKeyPKCS1);
            StringWriter stringWriter = new StringWriter();
            PemWriter pemWriter = new PemWriter(stringWriter);
            pemWriter.writeObject(pemObject);
            pemWriter.close();
            String pemString = stringWriter.toString();
            Log.println(Log.DEBUG, "PublicKey to PEM", pemString);

            return pemString;
        }catch(IOException e){
            e.printStackTrace();
            return null;
        }
    }


    //Receives a PEM in PKCS8 as String and generate a public key
    public PublicKey GeneratePublicKeyFromPEM(String pem) {
        try {
            //Security.addProvider(new BouncyCastleProvider());

            Log.println(Log.DEBUG, "Generating PubKey from:", pem);
            Reader rdr = new StringReader(pem);

            PemObject spki = new PemReader(rdr).readPemObject();
            PublicKey key = KeyFactory.getInstance(KeyProperties.KEY_ALGORITHM_RSA).generatePublic(new X509EncodedKeySpec(spki.getContent()));

            showPublicKeyModulusAndPublicExponent(key);

            return key;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void showPublicKeyModulusAndPublicExponent(PublicKey pubKey){
        String publicKeyStringProf;
        byte[] modulus = ((RSAPublicKey)pubKey).getModulus().toByteArray();
        publicKeyStringProf = "Modulus (" + modulus.length + "):\n";
        publicKeyStringProf += byteArrayToHex(modulus) + "\n";
        publicKeyStringProf += "Public Exponent:\n";
        publicKeyStringProf += byteArrayToHex(((RSAPublicKey)pubKey).getPublicExponent().toByteArray()) + "\n";
        //Log.println(Log.ASSERT,"Key do Prof", publicKeyStringProf);
    }

    public String byteArrayToHex(byte[] ba) {
        StringBuilder sb = new StringBuilder(ba.length * 2);
        for(byte b: ba)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public PublicKey getPubKeyFromAndroidKeyStore(String username)  {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            PublicKey publicKey = keyStore.getCertificate(KEY_ALIAS+username).getPublicKey();
            return publicKey;
        }catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e){
            e.printStackTrace();
            return null;
        }
    }

    public PrivateKey getPriKeyFromAndroidKeyStore(String username)  {
        try {
            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            KeyStore.Entry entry = keyStore.getEntry(KEY_ALIAS+username, null);
            PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) entry).getPrivateKey();
            return privateKey;
        }catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableEntryException e){
            e.printStackTrace();
            return null;
        }
    }

    public boolean checkIfKeyExists(String username)  {
        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);

            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                //Printing all the key aliases in Android KeyStore
                System.out.println("Alias: " + aliases.nextElement());
            }
            KeyStore.Entry entry = ks.getEntry(KEY_ALIAS + username, null);
            if (!(entry instanceof KeyStore.PrivateKeyEntry)) {
                System.out.println("Not an instance of a PrivateKeyEntry");
                return false;
            }
            return true;
        }catch(KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableEntryException e){
            e.printStackTrace();
            return false;
        }
    }

    public PublicKey getPubKey() {
        return pubKey;
    }

    public PrivateKey getPriKey() {
        return priKey;
    }

    public void setServerPubKey(PublicKey serverPubKey){
        this.serverPubKey = serverPubKey;
    }

    public PublicKey getServerPubKey(){
        return this.serverPubKey;
    }

    public void deleteKeyStoreEntries(){ //FOR TEST PURPOSES
        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);

            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                //Printing all the key aliases in Android KeyStore
                ks.deleteEntry(aliases.nextElement());
            }
            Log.println(Log.ASSERT, "DeleteKeysInKeyStore" , "All keys deleted!!");
        }catch(KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e){
            e.printStackTrace();
            return ;
        }

    }

    public void checkKeysInAndroidKeyStore() {
        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);

            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                //Printing all the key aliases in Android KeyStore
                Log.println(Log.ASSERT, "KeysInKeyStore" , aliases.nextElement());
                //System.out.println("Alias: " + aliases.nextElement());
            }
        }catch(KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e){
            e.printStackTrace();
            return ;
        }
    }

    public String encryptChallenge(UUID challenge){
        try {
            byte[] input = challenge.toString().getBytes();

            Cipher cipher = Cipher.getInstance(Constants.ENC_ALGO);
            cipher.init(Cipher.ENCRYPT_MODE, this.priKey);
            byte[] encChallenge = cipher.doFinal(input);

            String base64 =  Base64.encodeToString(encChallenge, Base64.DEFAULT);

            return base64;
        }
        catch (Exception e) {

            Log.println(Log.ASSERT, "test", e.toString());
            e.printStackTrace();
            return null;
        }
    }
}
