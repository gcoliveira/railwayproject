package com.example.railwayappinspector;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Ticket implements Comparable<Ticket> {
    private String uuid;
    private Station origin, destination;
    private String travelDate;
    private String travelTime;
    private boolean used;

    public Ticket(String uuid, Station origin, Station destination, String travelDate, boolean used, String time) {
        this.uuid = uuid;
        this.origin = origin;
        this.destination = destination;
        this.travelDate = travelDate;
        this.used = used;
        this.travelTime = time;

    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Station getOrigin() {
        return origin;
    }

    public void setOrigin(Station origin) {
        this.origin = origin;
    }

    public Station getDestination() {
        return destination;
    }

    public void setDestination(Station destination) {
        this.destination = destination;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public Date getTicketDateAsDateObject(){
        try {
            Date date = new SimpleDateFormat("HH:mm dd/MM/yyyy").parse(this.getTravelTime() + " " + this.getTravelDate());
            return date;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public int compareTo(Ticket t){
        if (getTicketDateAsDateObject() == null || t.getTicketDateAsDateObject() == null) {
            return 0;
        }
        return getTicketDateAsDateObject().compareTo(t.getTicketDateAsDateObject());

    }

    @Override
    public String toString() {
        return "Ticket{" +
                "origin=" + origin +
                ", destination=" + destination +
                ", travelDate=" + travelDate +
                ", used=" + used +
                '}';
    }
}
