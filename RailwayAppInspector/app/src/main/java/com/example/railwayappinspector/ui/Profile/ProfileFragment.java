package com.example.railwayappinspector.ui.Profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.railwayappinspector.MainActivity;
import com.example.railwayappinspector.MyApplication;
import com.example.railwayappinspector.R;
import com.github.nkzawa.socketio.client.Socket;


public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    private EditText nameEditTextProfile;
    private Button logoutButton;
    private Socket ioSocket;
    private SharedPreferences sharedPref;

    private MyApplication myApp;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        myApp = (MyApplication) getActivity().getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();

        nameEditTextProfile = root.findViewById(R.id.nameEditTextProfile);


        sharedPref = getActivity().getApplicationContext().getSharedPreferences("railwaySP", Context.MODE_PRIVATE);


        if(myApp.getInspector() != null){
            nameEditTextProfile.setText(myApp.getInspector().getName());
        }
        logoutButton = root.findViewById(R.id.logoutButton);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myApp.setInspector(null);
                myApp.setLoggedin(false);
                getActivity().recreate();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove(("uuid"));
                editor.commit();
            }
        });

        ((MainActivity)getActivity()).setFragmentRefreshListener(new MainActivity.FragmentRefreshListener() {
            @Override
            public void onRefresh() {
                refreshPage();
            }
        });

        return root;
    }

    private void refreshPage() {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                nameEditTextProfile.setText(myApp.getInspector().getName());
            }
        });
    }



    private void setListening() {

    }
}