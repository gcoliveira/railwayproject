package com.example.railwayappinspector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Stop {
    private int stopId;
    private String stopTime;
    private int stationNumber;
    private int nextStationNumber;
    private int seatsToNextStop;
    private int tripId;


    public Stop(int stopId, String stopTime, int stationNumber, int nextStationNumber, int seatsToNextStop, int tripId) {
        this.stopId = stopId;
        this.stopTime = stopTime;
        this.stationNumber = stationNumber;
        this.nextStationNumber = nextStationNumber;
        this.seatsToNextStop = seatsToNextStop;
        this.tripId = tripId;
    }

    public int getStopId() {
        return stopId;
    }

    public void setStopId(int stopId) {
        this.stopId = stopId;
    }

    public String getStopTime() {
        return stopTime;
    }

    public int getStopTimeHour(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH");
        SimpleDateFormat dateFormatMinute = new SimpleDateFormat("mm");
        try {
            Date date = dateFormat.parse(stopTime);


            String minutes = dateFormatMinute.format(date);
            String hours = dateFormatHour.format(date);
            return Integer.parseInt(hours);
        } catch (ParseException e) {
            return 0;
        }
    }

    public int getStopTimeMinute(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH");
        SimpleDateFormat dateFormatMinute = new SimpleDateFormat("mm");
        try {
            Date date = dateFormat.parse(stopTime);


            String minutes = dateFormatMinute.format(date);
            String hours = dateFormatHour.format(date);
            return Integer.parseInt(minutes);
        } catch (ParseException e) {
            return 0;
        }

    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public int getStationNumber() {
        return stationNumber;
    }

    public void setStationNumber(int stationNumber) {
        this.stationNumber = stationNumber;
    }

    public int getNextStationNumber() {
        return nextStationNumber;
    }

    public void setNextStationNumber(int nextStationNumber) {
        this.nextStationNumber = nextStationNumber;
    }

    public int getSeatsToNextStop() {
        return seatsToNextStop;
    }

    public void setSeatsToNextStop(int seatsToNextStop) {
        this.seatsToNextStop = seatsToNextStop;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    @Override
    public String toString() {
        return "Stop{" +
                "stopId='" + stopId + '\'' +
                "stopTime='" + stopTime + '\'' +
                ", stationNumber=" + stationNumber +
                ", nextStationNumber=" + nextStationNumber +
                ", seats=" + seatsToNextStop +
                ", trip=" + tripId +
                '}';
    }
}
