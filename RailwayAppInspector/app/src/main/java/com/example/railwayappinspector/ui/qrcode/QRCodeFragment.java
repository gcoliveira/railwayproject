package com.example.railwayappinspector.ui.qrcode;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.railwayappinspector.Confirmation;
import com.example.railwayappinspector.Constants;
import com.example.railwayappinspector.MainActivity;
import com.example.railwayappinspector.MyApplication;
import com.example.railwayappinspector.R;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.security.Signature;

import javax.crypto.Cipher;

public class QRCodeFragment extends Fragment {

    private QRCodeViewModel QRCodeViewModel;
    private TextView resultFromScanTextView;
    private RelativeLayout loadingPanelQRActivity;
    private LinearLayout selectedTripLinearLayout, ticketConfirmationLinearLayout;
    private Socket ioSocket;
    private TextView originStationTitle, destinationStationTitle, originStationHourTitle, selectedDateTitle, destinationStationHourTitle, validatedTicketsTextView, existingTicketsTextView, errorTextView;
    private TextView ticketUuidTextView, userUuidTextView, originTextView, destinationTextView, timeTextView, dateTextView, stateTextView; //ticket fields
    private SurfaceView surfaceView;
    MyApplication myApp;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private Button backFromScanButton;
    private static final int REQUEST_CAMERA_PERMISSION = 201;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        QRCodeViewModel =
                ViewModelProviders.of(this).get(QRCodeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_qr_code, container, false);

        myApp = (MyApplication)getActivity().getApplicationContext();
        ioSocket = myApp.getSocket();
       // setListening();


        //selected trip
        originStationTitle = root.findViewById(R.id.originStationTitle);
        destinationStationTitle = root.findViewById(R.id.destinationStationTitle);
        originStationHourTitle = root.findViewById(R.id.originStationHourTitle);
        selectedDateTitle = root.findViewById(R.id.selectedDateTitle);
        destinationStationHourTitle = root.findViewById(R.id.destinationStationHourTitle);
        validatedTicketsTextView = root.findViewById(R.id.validatedTicketsTextView);
        existingTicketsTextView = root.findViewById(R.id.existingTicketsTextView);
        selectedTripLinearLayout = root.findViewById(R.id.selectedTripLinearLayout);

        //loading
        loadingPanelQRActivity = root.findViewById(R.id.loadingPanelQRActivity);

        //scanner
        errorTextView = root.findViewById(R.id.errorTextView);
        surfaceView = root.findViewById(R.id.surfaceView);
        //surfaceView.setMinimumHeight(surfaceView.getWidth()*2);

        //ticket
        ticketConfirmationLinearLayout = root.findViewById(R.id.ticketConfirmationLinearLayout);
        ticketUuidTextView = root.findViewById(R.id.ticketUuidTextView);
        userUuidTextView = root.findViewById(R.id.userUuidTextView);
        originTextView = root.findViewById(R.id.originTextView);
        destinationTextView = root.findViewById(R.id.destinationTextView);
        timeTextView = root.findViewById(R.id.timeTextView);
        dateTextView = root.findViewById(R.id.dateTextView);
        stateTextView = root.findViewById(R.id.stateTextView);
        backFromScanButton = root.findViewById(R.id.backFromScanButton);
        backFromScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ticketConfirmationLinearLayout.setVisibility(View.GONE);
                errorTextView.setVisibility(View.GONE);
                surfaceView.setVisibility(View.VISIBLE);

                backFromScanButton.setVisibility(View.GONE);
            }
        });

        if(myApp.getSelectedTrip()!=null) {
            errorTextView.setVisibility(View.GONE);
            setSelectedTrip();
            initialiseDetectorsAndSources();
        }else{
            errorTextView.setVisibility(View.VISIBLE);
            surfaceView.setVisibility(View.GONE);
        }

        return root;
    }

    private void setSelectedTrip(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                existingTicketsTextView.setText(myApp.getExisting()+" Existing Tickets");
                validatedTicketsTextView.setText(myApp.getValidated()+" Validated Tickets");
                originStationTitle.setText(myApp.getStationByNumber(myApp.getSelectedTrip().getInitialStationNumber()).getName());
                destinationStationTitle.setText(myApp.getStationByNumber(myApp.getSelectedTrip().getFinalStationNumber()).getName());
                originStationHourTitle.setText(myApp.getSelectedTrip().getInitialTime());
                destinationStationHourTitle.setText(myApp.getSelectedTrip().getFinalTime());
                selectedDateTitle.setText(myApp.getSelectedTripDate());
            }
        });
    }

    private void initialiseDetectorsAndSources() {

        Toast.makeText(getActivity().getApplicationContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();

        barcodeDetector = new BarcodeDetector.Builder(getActivity())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        cameraSource = new CameraSource.Builder(getActivity(), barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(surfaceView.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                Toast.makeText(getActivity().getApplicationContext(), "QR Code detected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() != 0) {


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            release();
                            cameraSource.stop();
                            surfaceView.setVisibility(View.GONE);
                            loadingPanelQRActivity.setVisibility(View.VISIBLE);

                        }
                    });

                    processQRCode(barcodes.valueAt(0).displayValue);
                    //errorTextView.setText(barcodes.valueAt(0).valueFormat + " - " + intentData);




                }
            }
        });
    }

    public void processQRCode(String qr_content){

        Log.println(Log.ASSERT, "received qrcode:", qr_content + "; LENGTH: "+qr_content.length());

        qr_content = qr_content.trim();
        Log.println(Log.ASSERT, "VALUES", "qr_content: "+qr_content +"; size: "+qr_content.length());


        byte[] all_ticket = Base64.decode(qr_content.getBytes(), Base64.DEFAULT);
        Log.println(Log.ASSERT, "VALUES", "byte_content: "+all_ticket +"; size: "+all_ticket.length);

        //byte[] all_ticket = intentData.getBytes(StandardCharsets.ISO_8859_1);
        byte[] uuidByte = new byte[36];
        for(int i = 0; i < 36; i++)
            uuidByte[i] = all_ticket[i];

        byte[] ticketByte = new byte[128];
        for(int i = 0; i < 128; i++)
            ticketByte[i] = all_ticket[i+36];

        byte[] signatureByte = new byte[64];
        for(int i = 0; i < 64; i++)
            signatureByte[i] = all_ticket[i+36+128];

        String ticketUUID = new String(uuidByte, StandardCharsets.ISO_8859_1);




        Log.println(Log.ASSERT, "processQRCode", "UUID SIZE:"+ticketUUID.length());


        Log.println(Log.ASSERT, "processQRCode", "ticket LENGTH:"+ticketByte.length);
        Log.println(Log.ASSERT, "processQRCode", "SIG LENGTH:"+signatureByte.length);

        PublicKey keyForThisTicket = null;

        Log.println(Log.ASSERT, "confirmations size", ":"+myApp.getConfirmations().size());
        int limit = 0;
        for(Confirmation c : myApp.getConfirmations()){
            if(ticketUUID.equalsIgnoreCase(c.getTicket().getUuid())){
                keyForThisTicket = c.getPubKey();
                limit = 79 + c.getTicket().getTravelDate().length();
                break;
            }
        }

        desyncriptThings(ticketByte, signatureByte, keyForThisTicket, limit);

    }


    public void desyncriptThings(byte[] encryptedThing, byte[] signature, PublicKey userPubKey, int limit){
        try {
            Cipher cipher = Cipher.getInstance(Constants.ENC_ALGO);
            cipher.init(Cipher.DECRYPT_MODE, userPubKey);
            //byte[] cypher_bytes = Base64.decode((byte[])args[0], Base64.DEFAULT);
            Log.println(Log.ASSERT, "desyncriptThings", "LENGTH:"+encryptedThing.length);
            byte[] clearTicket = cipher.doFinal(encryptedThing);



            ByteBuffer bTicket = ByteBuffer.wrap(clearTicket, 0, limit);
            byte[] firstuuid = new byte[36];
            byte[] seconduuid = new byte[36];
            bTicket.get(firstuuid, 0, 36);
            bTicket.get(seconduuid, 0, 36);
            String uuuid = new String(firstuuid, StandardCharsets.UTF_8);
            String tuuid = new String(seconduuid, StandardCharsets.UTF_8);
            Log.println(Log.ASSERT, "testing bytebuffers", uuuid);
            Log.println(Log.ASSERT, "testing bytebuffers", tuuid);

            String origin = new String(new byte[] { bTicket.get() });                                 // Origin Station nr (1..5)
            String dest = new String(new byte[] { bTicket.get() });                                     // Destination Station nr (1..5)

            byte[] timeByte = new byte[5];
            bTicket.get(timeByte, 0, 5);
            String time = new String(timeByte, StandardCharsets.UTF_8);


            Log.println(Log.ASSERT, "testing bytebuffers", "Origin:" + origin + "\nDestination: " + dest + "\nTime: "+time);

            byte[] dateByte = new byte[bTicket.array().length - bTicket.position()];
            bTicket.get(dateByte, 0, bTicket.array().length - bTicket.position());
            String date = new String(dateByte, StandardCharsets.UTF_8);



            Log.println(Log.ASSERT, "testing bytebuffers", "Origin:" + origin + "\nDestination: " + dest + "\nTime: "+time+" Date: "+date);


            Log.println(Log.ASSERT, "SERVER PUBKEY", myApp.getKeyManager().getPubKeyAsPEMString(myApp.getKeyManager().getServerPubKey()));

            Signature sg = Signature.getInstance(Constants.SIGN_ALGO);
            sg.initVerify(myApp.getKeyManager().getServerPubKey());
            sg.update(bTicket.array());
            boolean verified = sg.verify(signature);


            Log.println(Log.ASSERT, "Verified:", " "+verified);

            if(verified){
                ioSocket.emit("useTicket", tuuid);
                for(Confirmation c: myApp.getConfirmations()){
                    Log.println(Log.ASSERT, "confirmation state", "ticket id:"+c.getTicket().getUuid()+" - "+c.getTicket().isUsed());
                    if(c.getTicket().getUuid().equalsIgnoreCase(tuuid)) {
                        if (c.getTicket().isUsed()) {
                            showTicketResult(uuuid, tuuid, Integer.valueOf(origin), Integer.valueOf(dest), time, date, "Used Ticket!");
                            ;
                        }
                        else {
                            myApp.setValidated(myApp.getValidated()+1);
                            c.getTicket().setUsed(true);
                            showTicketResult(uuuid, tuuid, Integer.valueOf(origin), Integer.valueOf(dest), time, date, "Confirmed!");
                        }
                    }
                }
            }else{
                showTicketResult(uuuid, tuuid, Integer.valueOf(origin), Integer.valueOf(dest), time, date, "Wrong Signature!");
            }


        }
        catch (Exception e) {
            Log.println(Log.ASSERT, "Error ENCRYPTION", e.toString());
            e.printStackTrace();
            wrongTicket();
            return;
        }
    }

    private void showTicketResult(final String uuuid, final String tuuid, final int origin, final int dest, final String time, final String date, final String status) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ticketUuidTextView.setText(tuuid);
                userUuidTextView.setText(uuuid);
                originTextView.setText(myApp.getStationByNumber(origin).getName());
                destinationTextView.setText(myApp.getStationByNumber(dest).getName());
                timeTextView.setText(time);
                dateTextView.setText(date);
                stateTextView.setText(status);
                if(status.equalsIgnoreCase("Confirmed!")) {
                    validatedTicketsTextView.setText(myApp.getValidated()+" Validated Tickets");
                    stateTextView.setTextColor(ContextCompat.getColor(getActivity().getBaseContext(), R.color.colorPrimary));
                }
                else
                    stateTextView.setTextColor(ContextCompat.getColor(getActivity().getBaseContext(), R.color.colorPrimaryDark));

                backFromScanButton.setVisibility(View.VISIBLE);
                ticketConfirmationLinearLayout.setVisibility(View.VISIBLE);
                loadingPanelQRActivity.setVisibility(View.GONE);

            }
        });
    }

    private void wrongTicket() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                errorTextView.setText("Wrong Ticket!");
                errorTextView.setVisibility(View.VISIBLE);
                Log.println(Log.ASSERT, "TEST", "VISIBILITE GONE");
                loadingPanelQRActivity.setVisibility(View.GONE);
                backFromScanButton.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }


}
