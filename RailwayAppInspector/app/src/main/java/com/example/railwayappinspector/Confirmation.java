package com.example.railwayappinspector;

import androidx.annotation.NonNull;

import java.security.PublicKey;

public class Confirmation {

    private Ticket ticket;
    private String userUuid;
    private PublicKey pubKey;


    public Confirmation(Ticket ticket, PublicKey pubKey, String userUuid) {
        this.ticket = ticket;
        this.pubKey = pubKey;
        this.userUuid = userUuid;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public PublicKey getPubKey() {
        return pubKey;
    }

    public void setPubKey(PublicKey pubKey) {
        this.pubKey = pubKey;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @NonNull
    @Override
    public String toString() {
        return "Confirmation for ticket with uuid: "+ticket.getUuid()+"\nBelonging to user with uuid: "+userUuid+"\nAnd PubKey: "+pubKey.toString();
    }
}
