package com.example.railwayappinspector;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


    private MyApplication myApp;
    private Socket ioSocket;

    int existing = 0, validated = 0;



    public FragmentRefreshListener getFragmentRefreshListener() {return fragmentRefreshListener;}
    public void setFragmentRefreshListener(FragmentRefreshListener fragmentRefreshListener) {this.fragmentRefreshListener = fragmentRefreshListener; }
    private FragmentRefreshListener fragmentRefreshListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_trip, R.id.navigation_qrcode, R.id.navigation_nfc, R.id.navigation_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        myApp = (MyApplication) getApplicationContext();
        myApp.connect();
        ioSocket = myApp.getSocket();
        Log.println(Log.ASSERT, "SOCKET STUFF", ioSocket.id() + "; connected:"+ioSocket.connected());
        setListening();

        myApp.getKeyManager().checkKeysInAndroidKeyStore();

        if(!myApp.isLoggedin())
            ShowLoginPage();

    }

    private void ShowLoginPage() {
        Intent loginPage = new Intent(this, LoginActivity.class);
        startActivityForResult(loginPage, 1);
    }

    private void setHomePage() {
        //print the views with the user values
        if(getFragmentRefreshListener()!=null){
            Log.println(Log.ASSERT, "HERE WE", "GO");
            getFragmentRefreshListener().onRefresh();
        }
    }

    public void updateTicketsForCurrentTrip(Object... args){
        try {
            existing = 0; validated = 0;
            JSONArray tickets =  new JSONArray(args[0].toString());
            JSONArray userKeys =  new JSONArray(args[1].toString());
            for(int i=0; i < tickets.length(); i++){
                existing++;
                JSONObject tempTicket = tickets.getJSONObject(i);
                for(int j = 0; j < userKeys.length(); j++){
                    JSONObject tempUserKey = userKeys.getJSONObject(j);
                    if(tempTicket.getString("userUuid").equalsIgnoreCase(tempUserKey.getString("uuid"))){
                        Log.println(Log.ASSERT, "ADD CONFIRMATION", "ADDING ONE - i:"+i+"; j:"+j);
                        Log.println(Log.ASSERT, "ADD CONFIRMATION", "useruuid in ticket:"+tempTicket.getString("userUuid")+"; user uuid:"+tempUserKey.getString("uuid"));

                        boolean used;
                        if(tempTicket.getInt("used") == 1){ used = true; validated++;}
                        else used = false;
                        myApp.addConfirmation(new Confirmation(
                                        new Ticket(tempTicket.getString("ticketUuid"),
                                                myApp.getStationByNumber(tempTicket.getInt("initialStationNumber")),
                                                myApp.getStationByNumber(tempTicket.getInt("finalStationNumber")),
                                                tempTicket.getString("date"),
                                                used,
                                                tempTicket.getString("time")
                                        ),
                                        myApp.getKeyManager().GeneratePublicKeyFromPEM(tempUserKey.getString("RSAPublicKey")),
                                        tempUserKey.getString("uuid")
                                )
                        );
                        break;
                    }
                }
            }

        } catch (JSONException e) {
            Log.println(Log.ASSERT, "Erros:", e.toString());
            e.printStackTrace();
        }

        //for(int i = 0; i < myApp.getConfirmations().size(); i++)
        //    Log.println(Log.ASSERT, "Confirmations:", myApp.getConfirmations().get(i).toString());

        myApp.setExisting(existing);
        myApp.setValidated(validated);

    }

    private void setListening() {

        ioSocket.off("getInitialDataFromDB");
        ioSocket.off("loggedIn");
        ioSocket.off("returnTicketsAndKeys");

        ioSocket.on("getInitialDataFromDB", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                try {
                    myApp.clearData();
                    JSONArray stations =  new JSONArray(args[0].toString());
                    for(int i=0; i < stations.length(); i++){
                        JSONObject tempStation = stations.getJSONObject(i);
                        myApp.addStation(new Station(tempStation.getInt("stationNumber"), tempStation.getString("name")));
                        //Log.println(Log.ASSERT, "station received:", "Name: "+tempStation.getString("name")+ "; Number: "+tempStation.getInt("stationNumber"));
                    }

                    JSONArray stops =  new JSONArray(args[1].toString());
                    for(int i=0; i < stops.length(); i++){
                        JSONObject tempStop = stops.getJSONObject(i);
                        String test = tempStop.getString("nextStationNumber");
                        if(test == "null") {
                            myApp.addStop(new Stop(tempStop.getInt("stopId"), tempStop.getString("stopTime"),
                                    tempStop.getInt("stationNumber"), 0, tempStop.getInt("seatsToNextStop"), tempStop.getInt("tripId")));
                        }else{
                            myApp.addStop(new Stop(tempStop.getInt("stopId"), tempStop.getString("stopTime"),
                                    tempStop.getInt("stationNumber"), tempStop.getInt("nextStationNumber"), tempStop.getInt("seatsToNextStop"), tempStop.getInt("tripId")));
                        }
                    }

                    //for(Stop s:myApp.getStops())
                    //Log.println(Log.ASSERT, "trips:", s.toString());

                    JSONArray trips =  new JSONArray(args[2].toString());
                    for(int i=0; i < trips.length(); i++){
                        JSONObject tempTrip = trips.getJSONObject(i);
                        Trip aux = new Trip(tempTrip.getInt("tripId"), tempTrip.getInt("initialStationNumber"), tempTrip.getInt("finalStationNumber"),
                                tempTrip.getString("initialTime"), tempTrip.getString("finalTime"));

                        for(Stop s: myApp.getStops()){
                            if(s.getTripId() == tempTrip.getInt("tripId")) {
                                aux.addStop(s);
                            }
                        }
                        myApp.addTrip(aux);

                    }

                    //for(Trip t:myApp.getTrips())
                    //Log.println(Log.ASSERT, "trips:", t.toString());
                    setHomePage();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public interface FragmentRefreshListener{
        void onRefresh();
    }

}
