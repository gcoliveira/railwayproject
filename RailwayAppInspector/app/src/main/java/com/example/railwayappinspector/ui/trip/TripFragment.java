package com.example.railwayappinspector.ui.trip;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatViewInflater;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.railwayappinspector.Confirmation;
import com.example.railwayappinspector.MainActivity;
import com.example.railwayappinspector.MyApplication;
import com.example.railwayappinspector.R;
import com.example.railwayappinspector.Station;
import com.example.railwayappinspector.Ticket;
import com.example.railwayappinspector.Trip;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class TripFragment extends Fragment {

    private TripViewModel tripViewModel;
    private DatePicker tripCheckDatePicker;
    private Button changeDateButton, changeTripButton;
    private RelativeLayout loadingPanelTripActivity;
    private TableRow TableRowTest;
    private LinearLayout selectedTripLinearLayout, selectTripLinearLayout;
    private TableLayout tripsTableLayout;
    private Socket ioSocket;
    private TextView originStationTitle, destinationStationTitle, originStationHourTitle, selectedDateTitle, destinationStationHourTitle, validatedTicketsTextView, existingTicketsTextView;
    MyApplication myApp;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        tripViewModel = ViewModelProviders.of(this).get(TripViewModel.class);
        View root = inflater.inflate(R.layout.fragment_trip, container, false);

        myApp = (MyApplication)getActivity().getApplicationContext();
        ioSocket = myApp.getSocket();
        setListening();



        tripCheckDatePicker = root.findViewById(R.id.tripCheckDatePicker);
        Calendar calendar = Calendar.getInstance();
        tripCheckDatePicker.setMinDate(calendar.getTimeInMillis() - 1000);

        changeTripButton = root.findViewById(R.id.changeTripButton);


        selectTripLinearLayout = root.findViewById(R.id.selectTripLinearLayout);
        selectedTripLinearLayout = root.findViewById(R.id.selectedTripLinearLayout);



        changeTripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        originStationTitle = root.findViewById(R.id.originStationTitle);
        destinationStationTitle = root.findViewById(R.id.destinationStationTitle);
        originStationHourTitle = root.findViewById(R.id.originStationHourTitle);
        selectedDateTitle = root.findViewById(R.id.selectedDateTitle);
        destinationStationHourTitle = root.findViewById(R.id.destinationStationHourTitle);

        validatedTicketsTextView = root.findViewById(R.id.validatedTicketsTextView);
        existingTicketsTextView = root.findViewById(R.id.existingTicketsTextView);


        selectedTripLinearLayout = root.findViewById(R.id.selectedTripLinearLayout);
        loadingPanelTripActivity = root.findViewById(R.id.loadingPanelTripActivity);

        tripsTableLayout = root.findViewById(R.id.tripsTableLayout);

        changeTripButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTripButton.setVisibility(View.GONE);
                selectedTripLinearLayout.setVisibility(View.GONE);
                selectTripLinearLayout.setVisibility(View.VISIBLE);
                for(int i = 0; i < tripsTableLayout.getChildCount(); i++){
                    tripsTableLayout.getChildAt(i).setEnabled(true);
                }
            }
        });


        for(Trip t: myApp.getTrips()) {
            String name = myApp.getStationByNumber(t.getInitialStationNumber()).getName();

            Log.println(Log.ASSERT, name + "         TRIPS:", t.getInitialStationNumber()+" - "+t.getInitialTime()+"  ->  "+t.getFinalStationNumber()+" - "+t.getFinalTime());

            View view = LayoutInflater.from(getActivity().getBaseContext()).inflate(R.layout.table_row_linear_layout, null);

            TextView originStationTV = view.findViewById(R.id.originStationTV);
            TextView originTimeTV = view.findViewById(R.id.originTimeTV);

            originStationTV.setText(myApp.getStationByNumber(t.getInitialStationNumber()).getName());
            originTimeTV.setText(t.getInitialTime());

            TextView destinationStationTV = view.findViewById(R.id.destinationStationTV);
            TextView destinationTimeTV = view.findViewById(R.id.destinationTimeTV);

            destinationStationTV.setText(myApp.getStationByNumber(t.getFinalStationNumber()).getName());
            destinationTimeTV.setText(t.getFinalTime());

            view.setTag(t);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myApp.clearConfirmations();
                    Trip t = (Trip)v.getTag();

                    String date = tripCheckDatePicker.getDayOfMonth() + "/" + (tripCheckDatePicker.getMonth()+1) + "/" + tripCheckDatePicker.getYear();


                    myApp.setSelectedTrip(t);
                    myApp.setSelectedTripDate(date);
                    setSelectedTrip();

                    loadingPanelTripActivity.setVisibility(View.VISIBLE);
                    //selectedTripLinearLayout.setVisibility(View.VISIBLE);

                    for(int i = 0; i < tripsTableLayout.getChildCount(); i++){
                        tripsTableLayout.getChildAt(i).setEnabled(false);
                    }


                    getTickets();

                }
            });

            tripsTableLayout.addView(view);
        }

        if(myApp.getSelectedTrip() == null) {
            selectedTripLinearLayout.setVisibility(View.GONE);
            changeTripButton.setVisibility(View.GONE);
        }else {
            setSelectedTrip();
            selectTripLinearLayout.setEnabled(false);
            selectTripLinearLayout.setVisibility(View.GONE);

        }

        return root;
    }

    private void setSelectedTrip(){
        getActivity().runOnUiThread(new Runnable() {
        @Override
            public void run() {
            existingTicketsTextView.setText(myApp.getExisting()+" Existing Tickets");
            validatedTicketsTextView.setText(myApp.getValidated()+" Validated Tickets");
                originStationTitle.setText(myApp.getStationByNumber(myApp.getSelectedTrip().getInitialStationNumber()).getName());
                destinationStationTitle.setText(myApp.getStationByNumber(myApp.getSelectedTrip().getFinalStationNumber()).getName());
                originStationHourTitle.setText(myApp.getSelectedTrip().getInitialTime());
                destinationStationHourTitle.setText(myApp.getSelectedTrip().getFinalTime());
                selectedDateTitle.setText(myApp.getSelectedTripDate());
            }
        });
    }

    private void getTickets() {
        ioSocket.emit("getTicketsForTrip",  myApp.getSelectedTrip().getTripId(), myApp.getSelectedTripDate());
       // selectedTripLinearLayout.setVisibility(View.VISIBLE);
        //changeTripButton.setVisibility(View.VISIBLE);
    }

    private void setListening() {

        ioSocket.off("errorGettingTickets");
        ioSocket.off("returnTicketsAndKeys");
        ioSocket.off("noTicketsForTrip");

        ioSocket.on("returnTicketsAndKeys", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                ((MainActivity)getActivity()).updateTicketsForCurrentTrip(args);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        existingTicketsTextView.setText(myApp.getExisting()+" Existing Tickets");
                        validatedTicketsTextView.setText(myApp.getValidated()+" Validated Tickets");
                        selectedTripLinearLayout.setVisibility(View.VISIBLE);
                        loadingPanelTripActivity.setVisibility(View.GONE);
                        changeTripButton.setVisibility(View.VISIBLE);
                        selectTripLinearLayout.setVisibility(View.GONE);
                    }
                });
            }

        });

        ioSocket.on("errorGettingTickets", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        });

        ioSocket.on("noTicketsForTrip", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        existingTicketsTextView.setText("0 Existing Tickets");
                        validatedTicketsTextView.setText("0 Validated Tickets");
                        selectedTripLinearLayout.setVisibility(View.VISIBLE);
                        loadingPanelTripActivity.setVisibility(View.GONE);
                        changeTripButton.setVisibility(View.VISIBLE);
                        selectTripLinearLayout.setVisibility(View.GONE);

                    }
                });
            }
        });



    }

}
