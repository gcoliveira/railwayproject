package com.example.railwayappinspector;

import java.util.ArrayList;
import java.util.List;

public class Inspector {
    private String uuid, name;

    public Inspector(String uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    @Override
    public String toString(){
        return "name: " + name + " ["+uuid+"];";
    }


}
