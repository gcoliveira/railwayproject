const express = require('express'),
http = require('http'),
app = express(),
server = http.createServer(app),
io = require('socket.io').listen(server);
var mysql = require('mysql');
const bcrypt = require('bcrypt');
const fs = require('fs');
const NodeRSA = require('node-rsa');
var crypto = require("crypto");
var ByteBuffer = require("bytebuffer");
var seatsForEachStop = 2;


const serverPubKey = new NodeRSA();
const serverPriKey = new NodeRSA();

const pathPublicRsa = './PublicRSA.txt'
const pathPrivateRsa = './PrivateRSA.txt'

try {
  if (fs.existsSync(pathPublicRsa) && fs.existsSync(pathPrivateRsa)) {

    //console.log("The key files already exist!");
    fs.readFile(pathPublicRsa, 'utf8', function(err, data) {
        if (err) throw err;
        serverPubKey.importKey(data, "pkcs1-public-pem");
        console.log('\n\nRead public key from file:\n' + serverPubKey.exportKey('pkcs1-public-pem'));
        serverPubKey.setOptions({encryptionScheme: 'pkcs1', signingScheme: 'pkcs1-sha256'});
    });
    
    fs.readFile(pathPrivateRsa, 'utf8', function(err, data) {
        if (err) throw err;
        serverPriKey.importKey(data, "pkcs1-private-pem");
        console.log('\nRead private key from file:\n' + serverPriKey.exportKey('pkcs1-private-pem') + '\n\n');
        serverPriKey.setOptions({encryptionScheme: 'pkcs1', signingScheme: 'pkcs1-sha256'});
    });

  }else{

    console.log('Missing a key file');

    const serverKeys = new NodeRSA();
    serverKeys.generateKeyPair(1024)//, 10001)

    fs.writeFile(pathPublicRsa, serverKeys.exportKey('pkcs1-public-pem'), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The Public Key was saved!");
    }); 
    
    fs.writeFile(pathPrivateRsa, serverKeys.exportKey('pkcs1-private-pem'), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The Private Key was saved!");
    }); 

    serverPubKey.importKey(serverKeys.exportKey('pkcs1-public-pem'), 'pkcs1-public-pem');
    serverPriKey.importKey(serverKeys.exportKey('pkcs1-private-pem'), 'pkcs1-private-pem');
    serverPubKey.setOptions({encryptionScheme: 'pkcs1', signingScheme: 'pkcs1-sha256'});
    serverPriKey.setOptions({encryptionScheme: 'pkcs1', signingScheme: 'pkcs1-sha256'});

  }
} catch(err) {
  console.error(err)
}


const uuidv5 = require('uuid/v5')
const uuidv4 = require('uuid/v4')

const MY_NAMESPACE = '9201799e-59cd-4dbc-babb-aed421cbee08';

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "railwaydb"
  });

app.get('/', (req, res) => {
    res.send('Chat Server is running on port 3000')
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected to data base!");
});


server.listen(3000,()=>{
    console.log('Node app is running on port 3000')
});

function updateUserKey(receivedPEM, username){
    return new Promise(function(resolve, reject){
        console.log('Received PEM to update '+username);
        con.query('UPDATE user SET RSAPublicKey = \''+receivedPEM+'\' WHERE name = \''+username+'\'',function(err,rows){
            if(err) {
                console.log("Error updating key in user with name "+name+": "+err);
                resolve(false);
            }else{
                console.log('user '+username+' got his key updated');
                resolve(true);
            }
        });
    });
}

function compareKeys(receivedPEM, storedPEM){
    return new Promise(function(resolve, reject){
        console.log('[COMPARE KEYS]\nReceived PEM: \n'+receivedPEM+'\n\nStored PEM:\n'+storedPEM);
        if(receivedPEM === storedPEM){
            console.log('[COMPARE KEYS]: equal');
            resolve(true);
        }else{
            console.log('[COMPARE KEYS]: different');
            resolve(false);
        }
    });
}

function addUser(name, email, password, nationality, date, pubkey){
    return new Promise(function(resolve, reject){
        var uuid = uuidv5(name, MY_NAMESPACE);
        console.log('UUID created for user '+name+': ' + uuid)
        con.query('INSERT INTO user(uuid, name, email, password, nationality, birthday, RSAPublicKey)' +
        'VALUES(\''+uuid+'\', \''+name+'\', \'' + email + '\', \'' + password + '\', \'' + nationality + '\', \'' + date + '\', \''+pubkey+'\')', function (err, result) {
            if(err) {
                if(err.code.trim() === 'ER_DUP_ENTRY')
                    console.log("Duplicate Values");
                else
                    console.log("error: ", err);
                resolve(false);
            }else{
                console.log("User registered");
                resolve(uuid);
            }
        });
    });
}

function addCard(cardNumber, cardType, cardValidity, uuid){
    return new Promise(function(resolve, reject){
        con.query('INSERT INTO creditcard(number, type, validity, userUuid)' +
        'VALUES(\''+cardNumber+'\', \''+cardType+'\', \'' + cardValidity + '\', \''+ uuid + '\')', function (err, result) {
            if(err) {
                if(err.code.trim() === 'ER_DUP_ENTRY')
                    console.log("Duplicate Card Values");
                else
                    console.log("error: ", err);
                resolve(false);
            }else{
                console.log("Card added");
                resolve(true);
            }
        });
    });
}

function getUserByUuid(uuid){
    return new Promise(function(resolve, reject){
        con.query('SELECT * FROM user WHERE uuid = \''+uuid+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with uuid "+uuid+": "+err);
                resolve(false);
            }else{
                if (!rows.length){
                    console.log('Got no user with uuid: '+uuid)
                    resolve(false);
                }
                else{
                    resolve(rows[0]);
                }
            }
        });
    });

}

function getUser(name){
    return new Promise(function(resolve, reject){
        con.query('SELECT * FROM user WHERE name = \''+name+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with name "+name+": "+err);
                resolve(false);
            }else{
                if (!rows.length){
                    console.log('Got no user with name: '+name)
                    resolve(false);
                }
                else{
                    resolve(rows[0]);
                }
            }
        });
    });

}

function checkSeats(originStopId, finalStopId, date){
    return new Promise(function (resolve, reject){

        var promiseArray = [];
        var seatsLeft = true;
        console.log('check seats');
        for (var i=0; i < finalStopId - originStopId; i++) {
            console.log('check seats for stop '+ (originStopId+i));
            promiseArray.push(checkSeatsForStop((originStopId+i), date));
            console.log('got all the seats from stop '+(originStopId+i));
            
        }

        Promise.all(promiseArray).then(function(data){
            console.log("all promises: " +data)
            for(var i = 0; i < data.length; i++){
                if(data[i] === true){
                    console.log(i+': '+data[i]+" = true")
                }else{
                    console.log(i+': '+data[i]+" = false")
                    resolve(false)
                }
            }
            resolve(true)
        })

    });
}


function checkSeatsForStop(stopId, date){
    return new Promise(function (resolve, reject){
        console.log('trying to get all the seats from stop '+(stopId));
        con.query('SELECT * FROM seat WHERE stopId = '+(stopId)+' AND date = \''+date+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting seats with: "+err);
                resolve(false);
            }else{
                if (rows.length >= seatsForEachStop){
                    console.log('No Seats Left for stop '+stopId)
                    resolve(false);
                    //break;
                }
                else{
                    console.log('There\'s Seats Left for stop '+stopId)
                    resolve(true);
                }
            }
        });
    })

}


function getUserUuidAndRSAKey(userUuid){
    return new Promise(function (resolve, reject){
        console.log('trying to get key from user '+(userUuid));
        con.query('SELECT uuid,RSAPublicKey FROM user WHERE uuid = \''+userUuid+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting key from user: "+err);
                resolve(false);
            }else{
                if (!rows.length){
                    console.log("Error selecting key from user: "+err);
                    resolve(false);
                }
                else{
                    console.log('Got a key back for user '+userUuid)
                    const key = new NodeRSA();
                    key.importKey(rows[0].RSAPublicKey, 'pkcs1-public-pem')
                    rows[0].RSAPublicKey = key.exportKey('pkcs8-public-pem')
                    resolve(rows[0]);
                }
            }
        });
    })

}

function getUserPubKeysFromTickets(tickets){
    return new Promise(function (resolve, reject){

        var promiseArray = [];
        for (var i=0; i < tickets.length; i++) {
            console.log('get user key for ticket '+ (tickets[i].ticketUuid));
            promiseArray.push(getUserUuidAndRSAKey(tickets[i].userUuid));
            console.log('got the key for ticket '+(tickets[i].ticketUuid));
        }

        var result = []

        Promise.all(promiseArray).then(function(data){
            console.log("all promises: " +data)
            for(var i = 0; i < data.length; i++){
                if(data[i] === false){
                    console.log('mistakes happened in selecting keys')
                }else{
                    console.log('adding '+data[i]+' to selected users')
                    result.push(data[i]);
                }
            }
            resolve(result)
        })

    });

}



io.on('connection', (socket) => {
    var sentChallenge = null;

    console.log('User connected ' + socket.id)

    
    socket.on('join', function(userNickname) {
    
        console.log(userNickname +" : has joined the chat "  )

    });

  
    const testKey = NodeRSA();
   //a usar na testKeyStuff app
    socket.on('sendPubRSA', function(pubkey) {
        //console.log('received a rsa pub key PEM:\n'+pubkey);
        testKey.importKey(pubkey, 'pkcs1-public-pem');
        testKey.setOptions({encryptionScheme: 'pkcs1'});
        serverPriKey.setOptions({encryptionScheme: 'pkcs1', signingScheme: 'pkcs1-sha256'});
        serverPubKey.setOptions({encryptionScheme: 'pkcs1', signingScheme: 'pkcs1-sha256'});
        

        //console.log('key size:' + testKey.getKeySize() + '\npkcs1:' + testKey.exportKey('pkcs1-public-pem'));
        //console.log('pkcs8:' + testKey.exportKey('pkcs8-public-pem'));

        var challenge = uuidv4();

        socket.emit('receivePubRSA', serverPubKey.exportKey('pkcs8-public-pem'), challenge);
    });


    socket.on('removeCreditCard', function(creditCardNumber) {
        con.query('DELETE FROM creditcard WHERE number = \'' + creditCardNumber + '\'', function (err, result) {
            if(err) {
                console.log("error: ", err);
            }else{
                console.log("CreditCard ["+ creditCardNumber +"] removed");
                socket.emit('removeCreditCardSuccess');
            }
        });
    });

    socket.on('createReview', function(ticketUuid, rating, description) {
        console.log('got values for review:'+ticketUuid+'; rating:'+rating+' - '+description);
        con.query('INSERT INTO review(ticketUuid, rating, description)'+  
        'VALUES(\''+ticketUuid+'\', \''+rating+'\', \''+description+'\')', function(err,rows){
            if(err) {
                console.log("Error inserting review with ticketuuid "+ticketUuid+": "+err);
                socket.emit('createReviewError', err);
            }else{
                socket.emit('reviewCreated');
                
            }
        });
    });

    socket.on('isTicketReviewed', function(ticketUuid) {
        con.query('SELECT * FROM review WHERE ticketUuid = \'' + ticketUuid + '\'', function(err,rows){
            if(err) {
                console.log("Error selecting review with ticketuuid "+ticketUuid+": "+err);
                socket.emit('returnNoReview');
            }else{
                if (!rows.length){
                    console.log('Got no review for ticket '+ticketUuid)
                    socket.emit('returnNoReview');
                }
                else{
                    socket.emit('returnReview', rows[0].rating, rows[0].description);
                }
            }
        });
    });


    socket.on('buyTicket', function(userUUID, originStationNumber, finalStationNumber, initStop, finalStop, date, price, initTime){
        checkSeats(initStop, finalStop, date).then(response=>{
            console.log('testing return from for: ' + response)
            if(response === false){
                socket.emit('ticketPurchaseError', 'No seats Left');
                return;
            }

            var ticketUUID = uuidv4();
            console.log('adding ticket: '+ 'VALUES(\''+ticketUUID+'\', \''+userUUID+'\', ' + originStationNumber + ', ' + finalStationNumber + ', \'' + price + '\', 0,\'' + date + '\')');
            con.query('INSERT INTO ticket(ticketUuid, userUuid, initialStationNumber, finalStationNumber, price, used, date, time)' +
            'VALUES(\''+ticketUUID+'\', \''+userUUID+'\', ' + originStationNumber + ', ' + finalStationNumber + ', \'' + price + '\', 0,\'' + date + '\', \''+initTime+'\')', function (err, result) {
                if(err) {
                    console.log("ticketPurchaseError");
                    socket.emit('ticketPurchaseError', err);
                    if(err.code.trim() === 'ER_DUP_ENTRY')
                        console.log("Duplicate Values");
                    else
                        console.log("error: ", err);
                }else{
                    console.log("Ticket bought");
                    socket.emit('ticketPurchaseComplete');

                    for (var i=0; i < finalStop - initStop; i++) {
                        console.log('creating seat for stop:'+(initStop+i));
                        con.query('INSERT INTO seat(ticketUuid, stopId, date) VALUES(\''+ticketUUID+'\', \''+(initStop+i)+'\', \'' + date + '\')', function (err, result) {
                            if(err) {
                                console.log('error creating seat:'+err)
                            }else{
                                console.log("Seat created");
                            }
                        });
                    }

                }
            });
        });

    });


    socket.on('addNewCard', function(cardNumber, cardType, cardValidity, userUUID){
        console.log('adding credit card: '+ 'VALUES(\''+cardNumber+'\', \''+cardType+'\', \'' + cardValidity + '\', \'' + userUUID + '\')');
        con.query('INSERT INTO creditcard(number, type, validity, userUuid)' +
        'VALUES(\''+cardNumber+'\', \''+cardType+'\', \'' + cardValidity + '\', \'' + userUUID + '\')', function (err, result) {
            if(err) {
                if(err.code.trim() === 'ER_DUP_ENTRY'){
                    console.log("Duplicate Values");
                    socket.emit('addNewCardError', 'Card Number already on use');
                }
                else{
                    console.log("error: ", err);
                    socket.emit('addNewCardError', err);
                }
            }else{
                console.log("CreditCard added");
                socket.emit('addNewCardSuccess', cardType, cardValidity, cardNumber);
            }
        });

    });

    socket.on('getUsedTickets', function(userUUID){
        console.log('getting used tickets for user: '+ userUUID );
        con.query('SELECT * FROM ticket WHERE userUuid = \''+userUUID +'\' AND used = 1', function(err, rows){
            if(err) {
                socket.emit('noUsedTickets');
                console.log("Error selecting used tickets for user "+userUUID+": "+err);
            }else{
                if (!rows.length){
                    console.log('No used Tickets')
                    socket.emit('noUsedTickets');
                }
                else{
                    var usedTickets = rows;
                    socket.emit('gotUsedTickets', usedTickets);
                }
            }
        });

    });

    socket.on('getUnusedTickets', function(userUUID){
        console.log('getting unused tickets for user: '+ userUUID );
        con.query('SELECT * FROM ticket WHERE userUuid = \''+userUUID +'\' AND used = 0', function(err, rows){
            if(err) {
                socket.emit('noUnusedTickets');
                console.log("Error selecting unused tickets for user "+userUUID+": "+err);
            }else{
                if (!rows.length){
                    console.log('No unused Tickets')
                    socket.emit('noUnusedTickets');
                }
                else{
                    var unusedTickets = rows;
                    socket.emit('gotUnusedTickets', unusedTickets);
                }
            }
        });

    });
    

    socket.on('sendTicketData', function(userUUID, initStation, finStation, date) {

        //console.log('received: '+userUUID+' '+initStation+'-'+finStation + ': '+date)
        var TicketUUID = uuidv4();
        var bbufer = new ByteBuffer(36 + 36 + 1 + 1 + date.length); 
        bbufer.append(userUUID);
        bbufer.append(TicketUUID);
        bbufer.append(initStation);
        bbufer.append(finStation);
        bbufer.append(date);

        var signature = serverPriKey.sign(bbufer.buffer);

        //var verification = serverPubKey.verify(bbufer.buffer, signature);

        //console.log('result:' + verification);

        var encryptedTicket = testKey.encrypt(bbufer.buffer);

        console.log('server pub key: ' + serverPubKey.exportKey('pkcs1-public-pem'));
        //console.log('signature: ' + signature.toString());// + '\nencrypted: ' + encryptedTicket);

        //socket.emit('receiveTicketDataInByteBuffer', bbufer.buffer, bbufer.limit);
        socket.emit('receiveTicketDataInByteBuffer', encryptedTicket, bbufer.limit, signature);
    });

    socket.on('useTicket', function(ticketUuid){
        con.query('UPDATE ticket SET used = true WHERE ticketUuid = \''+ticketUuid+'\'',function(err,rows){
            if(err) {
                console.log("Error updating state in ticket with uuid "+ticketUuid+": "+err);
            }else{
                console.log('ticket with uuid  '+ticketUuid+' got his status updated to USED');
            }
        });
    });

    socket.on('getEncTicketAndSignature', function(userUuid, ticketUuid, keyPubTest) {
        getUserByUuid(userUuid).then(function(result){
            var user = result;
            con.query('SELECT * FROM ticket WHERE ticketUuid = \''+ticketUuid +'\'', function(err, rows){ //
                if(err) {
                    socket.emit('noTicketFound');
                    console.log("Error selecting tickets with uuid "+ticketUuid+": "+err);
                }else{
                    if (!rows.length){
                        console.log('No ticket')
                        socket.emit('noTicketFound');
                    }
                    else{
                        var ticket = rows[0];
                        
                        console.log('ticket sizes - date: '+ticket.date.length)
                        console.log('ticket sizes - time: '+ticket.time.length)

                        var bbufer = new ByteBuffer(36 + 36 + 1 + 1 + ticket.time.length + ticket.date.length); // 
                        bbufer.append(userUuid);
                        bbufer.append(ticketUuid);
                        bbufer.append(ticket.initialStationNumber.toString());
                        bbufer.append(ticket.finalStationNumber.toString());
                        bbufer.append(ticket.time);
                        bbufer.append(ticket.date);

                        console.log('bufer length: '+bbufer.buffer.length+'; bufer limit:'+bbufer.limit)

                        var signature = serverPriKey.sign(bbufer.buffer);

                        
                        const userPubKey = NodeRSA();
                        userPubKey.importKey(user.RSAPublicKey, 'pkcs1-public-pem');
                        const testKeytest = NodeRSA();
                        testKeytest.importKey(keyPubTest, 'pkcs1-public-pem');
                        userPubKey.setOptions({encryptionScheme: 'pkcs1'});

                    

                        //console.log('first: ' + userPubKey.exportKey('pkcs8-public-pem'))
                        //console.log('second: ' + testKeytest.exportKey('pkcs8-public-pem'))

                        var encryptedTicket = userPubKey.encrypt(bbufer.buffer);

                        
                        console.log('encrypted ticket length: '+encryptedTicket.length)

                        //console.log('server pub key: ' + serverPubKey.exportKey('pkcs1-public-pem'));
                        //console.log('we did it')
                        console.log('signature: ' + signature.toString());// + '\nencrypted: ' + encryptedTicket);

                        //socket.emit('receiveTicketDataInByteBuffer', bbufer.buffer, bbufer.limit);
                        socket.emit('EncTicketAndSignature', encryptedTicket, bbufer.limit, signature);
                        
                    }
                }
            });
        });

        



    });
    
    socket.on('sendEncryptedTicket', function(encryptedTicket){
        console.log('received encrypted: '+encryptedTicket);
        var decryptedTicket = testKey.decryptPublic(encryptedTicket);
        console.log('DECRIPTED: '+decryptedTicket);

        var bbuffer = ByteBuffer.wrap(decryptedTicket);
        var uuid1 = bbuffer.readString(36);
        var uuid2 = bbuffer.readString(36);
        var byte1 = bbuffer.readByte();
        var byte2 = bbuffer.readByte();
        var byte3 = bbuffer.readByte();
        var byte4 = bbuffer.readByte();
        var byte5 = bbuffer.readByte();


        console.log('uuid1: ' + uuid1);
        console.log('uuid2: ' + uuid2);
        console.log('byte1: ' + byte1);
        console.log('byte2: ' + byte2);
        console.log('byte3: ' + byte3);
        console.log('byte4: ' + byte4);
        console.log('byte5: ' + byte5);
    });

    socket.on('sendEncryptedChallengeToServer', function(encryptedChallenge, name, fn){
        console.log('received encrypted challenge from user [' + name + ']: '+encryptedChallenge);
        getUser(name).then(function(result){
            if(result != false){
                console.log("Got key: "+result.RSAPublicKey );
                const userKey = NodeRSA();
                userKey.importKey(result.RSAPublicKey, 'pkcs1-public-pem');
                userKey.setOptions({encryptionScheme: 'pkcs1'});
                try{
                    var decrypted = userKey.decryptPublic(encryptedChallenge);
                    //var decryptedWithWrongKey = serverPubKey.decryptPublic(encryptedChallenge);
                    console.log('DECRyPTED: '+decrypted+'\nSentChallenge: '+sentChallenge.toString());
                    //var uuidDec = uuid.fromString(decrypted);
                    if(decrypted == sentChallenge.toString()){
                        console.log('returning true');
                        fn('true');
                    }else{
                        console.log('returning false');
                        fn('false');
                    }
                }catch(err){
                    console.log('Error decrypting with this key:'+err)
                    console.log('returning false');
                    fn('false');
                }
            }else{
                console.log('returning false');
                fn('false');
            }
        });


    });

    socket.on('loginAsInspector', function(name, password){
        con.query('SELECT * FROM inspector WHERE name = \''+name+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting inspector with name "+name+": "+err);
                socket.emit('loginFailed', 'Error selecting inspector with name '+name+': '+err);
            }else{
                if (!rows.length){
                    console.log('Got login failed for '+name+': no inspector with that username')
                    socket.emit('loginFailed', 'Name doesn\'t exist');
                }
                else{
                    var inspector = rows[0];
                    console.log('Got inspector with username:' + inspector.name + ' and password: '+inspector.password);
                    if(password === inspector.password){
                            socket.emit('loginSuccess', name, inspector.uuid,  serverPubKey.exportKey('pkcs8-public-pem')); //ADICIONAR DADOS de cartoes P METER NA APP
                    }
                    else{
                        console.log('Got login failed for '+user.name+': wrong password')
                        socket.emit('loginFailed', 'Wrong password');
                    }
                }
            }
        });

    });

    socket.on('loginAndCompareKeys', function(name, password, userPubKeyPEM){
        con.query('SELECT * FROM user WHERE name = \''+name+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with name "+name+": "+err);
                socket.emit('loginFailed', 'Error selecting user with name '+name+': '+err);
            }else{
                if (!rows.length){
                    console.log('Got login failed for '+name+': no user with that username')
                    socket.emit('loginFailed', 'Name doesn\'t exist');
                }
                else{
                    var user = rows[0];
                    console.log('Got user with username:' + user.name + ' and password: '+user.password);
                    if(password === user.password){
                        compareKeys(userPubKeyPEM, user.RSAPublicKey).then(function(result){
                            if(result!=true)
                                updateUserKey(userPubKeyPEM, user.name);
                            var challenge = uuidv4();
                            sentChallenge = challenge;
                            socket.emit('loginSuccess', user.uuid, user.email, user.nationality, user.birthday, serverPubKey.exportKey('pkcs8-public-pem'), challenge); //ADICIONAR DADOS de cartoes P METER NA APP
                        });
                    }
                    else{
                        console.log('Got login failed for '+user.name+': wrong password')
                        socket.emit('loginFailed', 'Wrong password');
                    }
                }
            }
        });

    });
    
    socket.on('loginAndUpdateKeys', function(name, password, userPubKeyPEM){
        con.query('SELECT * FROM user WHERE name = \''+name+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with name "+name+": "+err);
                socket.emit('loginFailed', 'Error selecting user with name '+name+': '+err);
            }else{
                if (!rows.length){
                    console.log('Got login failed for '+name+': no user with that username')
                    socket.emit('loginFailed', 'Name doesn\'t exist');
                }
                else{
                    var user = rows[0];
                    console.log('Got user with username:' + user.name + ' and password: '+user.password);
                    if(password === user.password){
                        updateUserKey(userPubKeyPEM, user.name);  
                        var challenge = uuidv4();           
                        sentChallenge = challenge;                   
                        socket.emit('loginSuccess', user.uuid, user.email, user.nationality, user.birthday, serverPubKey.exportKey('pkcs8-public-pem'), challenge); //ADICIONAR DADOS de cartoes P METER NA APP
                    }
                    else{
                        console.log('Got login failed for '+user.name+': wrong password')
                        socket.emit('loginFailed', 'Wrong password');
                    }
                }
            }
        });

    });

    socket.on('loginWithSameAccount', function(name, userPubKeyPEM){
        con.query('SELECT * FROM user WHERE name = \''+name+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with name "+name+": "+err);
                socket.emit('loginFailedWithSameAccount', 'Error selecting user with name '+name+': '+err);
            }else{
                if (!rows.length){
                    console.log('Got login failed with same account for '+name+': no user with that username')
                    socket.emit('loginFailedWithSameAccount', 'Didn\'t find user \''+name+'\' on login with same account');
                }
                else{
                    var user = rows[0];
                    console.log('Got user with username: ' + user.name );
                    compareKeys(userPubKeyPEM, user.RSAPublicKey).then(function(result){
                        if(result != false){
                            var challenge = uuidv4();
                            sentChallenge = challenge;
                            socket.emit('loginSuccessWithSameAccount', user.uuid, user.email, user.nationality, user.birthday, serverPubKey.exportKey('pkcs8-public-pem'), challenge); //ADICIONAR DADOS de cartoes P METER NA APP
                        }else{
                            socket.emit('loginFailedWithSameAccount', 'Keys didn\'t match on login with same account');
                        }
                    });
                }
            }
        });

    });

    socket.on('loginWithSameAccountInspector', function(name){
        con.query('SELECT * FROM inspector WHERE name = \''+name+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting inspector with name "+name+": "+err);
                socket.emit('loginFailedWithSameAccount', 'Error selecting inspector with name '+name+': '+err);
            }else{
                if (!rows.length){
                    console.log('Got login failed with same account for '+name+': no inspector with that username')
                    socket.emit('loginFailedWithSameAccount', 'Didn\'t find inspector \''+name+'\' on login with same account');
                }
                else{
                    var inspector = rows[0];
                    console.log('Got inspector with username: ' + inspector.name );
                    socket.emit('loginSuccessWithSameAccount', name, inspector.uuid, serverPubKey.exportKey('pkcs8-public-pem')); //ADICIONAR DADOS de cartoes P METER NA APP
                        
                }
            }
        });

    });


    socket.on('alreadyLoggedInInspector', function(uuid, fn){
        con.query('SELECT * FROM inspector WHERE uuid = \''+uuid+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting inspector with uuid"+uuid+": "+err);
            }else{
                if (!rows.length){
                    fn("No result");
                }
                else{
                    console.log('User has a previous login with name:' + rows[0].name);
                    fn(rows[0].name);
                }
            }
        });
    });

    socket.on('alreadyLoggedIn', function(uuid, fn){
        con.query('SELECT * FROM user WHERE uuid = \''+uuid+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with uuid"+uuid+": "+err);
            }else{
                if (!rows.length){
                    fn("No result");
                }
                else{
                    console.log('User has a previous login with name:' + rows[0].name);
                    fn(rows[0].name);
                }
            }
        });
    });

    socket.on('getTicketsForTrip', function(tripId, date){
        con.query('SELECT * FROM trip WHERE tripId = \''+tripId+'\'',function(err,rows){
            if(err) {
                console.log("Error selecting trip: "+err);
                socket.emit('errorGettingTickets');
            }else{
                if (!rows.length){
                    socket.emit('errorGettingTickets');
                }
                else{
                    console.log('Trip selected:' + rows[0].tripId);
                    trip = rows[0];
                    con.query('SELECT * FROM ticket WHERE date = \''+date+'\' AND time >= \''+trip.initialTime+'\' AND time <= \''+trip.finalTime+'\'',function(err,rows){
                        if(err) {
                            console.log("Error selecting trip: "+err);
                            socket.emit('errorGettingTickets');
                        }else{
                            if (!rows.length){
                                console.log('No tickets for this trip');
                                socket.emit('noTicketsForTrip');
                            }
                            else{
                                var tickets = rows;
                                console.log('Selected tickets:' + tickets.length + 'for date: '+date);
                                
                                getUserPubKeysFromTickets(tickets).then(response=>{
                                    try{
                                        console.log('testing return from for: ' + response)
                                        if(response === false){
                                            console.log('return from userPubKeys is false')
                                            socket.emit('errorGettingTickets');
                                            return;
                                        }

                                        console.log('Users selected for keys: '+response.length)
                                        console.log('First user selected for keys: '+response[0].RSAPublicKey)
                                        socket.emit('returnTicketsAndKeys', tickets, response);
                                    }catch(e){
                                        console.log('ERROR- '+e)
                                        socket.emit('errorGettingTickets');
                                    }

                                });
                                
                            }
                        }
                    });
                }
            }
        });
    });


    socket.on('prepareToGetInitialDataFromDB', function(userUuid){
        con.query('SELECT * FROM station ORDER BY stationNumber',function(err,rows){
            if(err) {
                console.log("Error selecting stations: "+err);
            }else{
                if (!rows.length){
                    socket.emit('getInitialDataFromDB', "error")
                }
                else{
                    console.log('Select stations, first:' + rows[0].name);
                    var stations = rows;
                    con.query('SELECT * FROM stop ORDER BY stopId',function(err,rows){
                        if(err) {
                            console.log("Error selecting stops: "+err);
                        }else{
                            if (!rows.length){
                                socket.emit('getInitialDataFromDB', "error")
                            }
                            else{
                                console.log('selected stops, first stop id:' + rows[0].stopId);
                                var stops = rows;
                                con.query('SELECT * FROM trip ORDER BY tripId',function(err,rows){
                                    if(err) {
                                        console.log("Error selecting trips: "+err);
                                    }else{
                                        if (!rows.length){
                                            socket.emit('getInitialDataFromDB', "error")
                                        }
                                        else{
                                            console.log('selected trips, first trip id:' + rows[0].tripId);
                                            var trips = rows;
                                            con.query('SELECT * FROM creditcard WHERE userUuid = \''+userUuid+'\'',function(err,rows){
                                                if(err) {
                                                    console.log("Error selecting credit cards: "+err);
                                                }else{
                                                    if (!rows.length){
                                                        socket.emit('getInitialDataFromDB', stations, stops, trips, null)
                                                    }
                                                    else{
                                                        console.log('selected credit cards for '+userUuid+', first credit card number:' + rows[0].number + ' [' + rows[0].validity + ']');
                                                        var creditCards = rows;
                                                        socket.emit('getInitialDataFromDB', stations, stops, trips, creditCards)                
                                                    }
                                                }
                                            });                 
                                        }
                                    }
                                });                 
                            }
                        }
                    });              
                }
            }
        });
    });

    socket.on('registerWithOutCard', function(name, email, password, nationality, date, pubkey){
        console.log("Got register Values: "+name+"; " + email + "; " + password + "; " + nationality + '; ' + date + '; \nPubKey:' + pubkey);
        
        con.query('SELECT * FROM user WHERE name = \''+name+'\' OR email = \'' + email + '\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with username"+name+": "+err);
            }else{
                if (!rows.length){
                    //console.log("Got no result");
                    addUser(name, email, password, nationality, date, pubkey).then(function(result){
                        console.log("result: "+result);
                        if(result != false){
                            uuid = result;
                            socket.emit('RegisterSuccess', uuid, serverPubKey.exportKey('pkcs8-public-pem'));
                        }
                    });
                }
                else{
                    if(rows[0].name === name)
                        socket.emit('nameNotAvailable', name);
                    else if(rows[0].email === email)
                        socket.emit('emailNotAvailable', email);
                }
            }
        });
    });

    socket.on('registerWithCard', function(name, email, password, nationality, date, pubkey, cardNumber, cardType, cardValidity){
        console.log("Got register Values: "+name+"; " + email + "; " + password + "; " + nationality + '; ' + date + '; \nPubKey:' + pubkey + '\n'+cardNumber+'; '+cardType+'; '+cardValidity);
        
        con.query('SELECT * FROM user WHERE name = \''+name+'\' OR email = \'' + email + '\'',function(err,rows){
            if(err) {
                console.log("Error selecting user with username \'"+name+"\', and email \'"+email+"\':"+err);
            }else{
                if (!rows.length){
                    con.query('SELECT * FROM creditcard WHERE number = \''+cardNumber+'\'', function(err, rows){
                        if(err) {
                            console.log("Error selecting card with number"+cardNumber+": "+err);
                        }else{
                            if (!rows.length){
                                addUser(name, email, password, nationality, date, pubkey).then(function(result){
                                    console.log("result: "+result);
                                    if(result != false){
                                        uuid = result;
                                        addCard(cardNumber, cardType, cardValidity, uuid).then(function(result){
                                            if(result === true)
                                                socket.emit('RegisterSuccess', uuid, serverPubKey.exportKey('pkcs8-public-pem'))
                                        });
                                    }
                                });
                            }else{
                                socket.emit('cardNotAvailable', cardNumber);
                            }
                        }
                    });
                }else{
                    if(rows[0].name === name)
                        socket.emit('nameNotAvailable', name);
                    else if(rows[0].email === email)
                        socket.emit('emailNotAvailable', email);
                }
            }
        });
    });


    socket.on('disconnect', function() {
        console.log( 'user has left ')
        //socket.broadcast.emit( "userdisconnect" ,' user has left')
    });

});

