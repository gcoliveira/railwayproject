'use strict';
var inquirer = require('inquirer');
var childProcess = require('child_process');

console.log('Hi, welcome to RailwayApp server');

var question = 
  {
    type: 'list',
    name: 'option',
    message: 'What do you need?',
    choices: ['Launch Server', 'Create database (will delete current one)', 'Try to Connect to Database', 'Create Inspector Account'],
    filter: function(val) {
      return val.toLowerCase();
    }
  }


function runScript(scriptPath, callback) {

    // keep track of whether callback has been invoked to prevent multiple invocations
    var invoked = false;

    var process = childProcess.execFileSync(scriptPath);

    // listen for errors as they may prevent the exit event from firing
    process.on('error', function (err) {
        if (invoked) return;
        invoked = true;
        callback(err);
    });

    // execute the callback once the process has finished running
    process.on('exit', function (code) {
        if (invoked) return;
        invoked = true;
        var err = code === 0 ? null : new Error('exit code ' + code);
        callback(err);
    });

}

function example(){
    inquirer.prompt(question).then(answer => {
    console.log('Option selected: '+answer.option);
    switch(answer.option){
        case 'launch server':
            console.log('1');
            break;
        case 'create database (will delete current one)':
            console.log('2');
            break;
        case 'try to connect to database':
            console.log('3');
            runScript('./connect_to_db.js', function (err) {
                if (err) throw err;
                console.log('finished running connect_to_db.js');
            });
            break;
        default:
            break;
    }
    console.log('\n\n')
    example();
    });
}

example();