var mysql = require('mysql');

var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "railwaydb"
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});
