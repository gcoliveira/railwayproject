var mysql = require('mysql');
const uuidv4 = require('uuid/v4')

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");

  console.log("This will delete the database if you already have one created")

  con.query("DROP DATABASE IF EXISTS railwaydb", function (err, result) {
    if (err) throw err;
    console.log("Database deleted");
  });


  con.query("CREATE DATABASE railwaydb", function (err, result) {
    if (err) throw err;
    console.log("Database created");
  });

  con.query("USE railwaydb", function (err, result) {
    if (err) throw err;
    console.log("Database selected");
  });


  //USER
  var sql = 'CREATE TABLE IF NOT EXISTS user (' +
                'uuid VARCHAR(255) NOT NULL UNIQUE,' +         
                'name VARCHAR(255) NOT NULL UNIQUE,' + 
                'nationality VARCHAR(255) NOT NULL,' + 
                'birthday VARCHAR(255) NOT NULL,' +
                'RSAPublicKey VARCHAR(500) NOT NULL UNIQUE,'+
                'email VARCHAR(255) NOT NULL UNIQUE,'+
                'password VARCHAR(255) NOT NULL,'+
                'PRIMARY KEY (name))';
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created User table");
  });


//INSPECTOR
  var sql = 'CREATE TABLE IF NOT EXISTS inspector (' +  
  'uuid VARCHAR(255) NOT NULL UNIQUE,' +     
  'name VARCHAR(255) NOT NULL UNIQUE,' + 
  'password VARCHAR(255) NOT NULL,'+
  'PRIMARY KEY (name))';
con.query(sql, function(err, result) {
if(err) throw err;
console.log("Created Inspector table");
});

  

  //CREDIT CARD
  var sql = 'CREATE TABLE IF NOT EXISTS creditCard (' +
            'type VARCHAR(255) NOT NULL,' +
            'number VARCHAR(255) NOT NULL UNIQUE PRIMARY KEY,' + 
            'validity VARCHAR(255) NOT NULL,' +
            'userUuid VARCHAR(255) NOT NULL,' +
            'FOREIGN KEY (userUuid) REFERENCES user(uuid))';
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created Credit Card table");
  });



  //STATION
  var sql = `CREATE TABLE IF NOT EXISTS station (
              stationNumber INT UNIQUE NOT NULL PRIMARY KEY,
              name VARCHAR(255) UNIQUE 
              )
              `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created Station table");
  });




  //STOP
  var sql = `CREATE TABLE IF NOT EXISTS Stop (
              stopId INT PRIMARY KEY NOT NULL,
              stopTime VARCHAR(255) NOT NULL,
              stationNumber INT NOT NULL,
              nextStationNumber INT,
              seatsToNextStop INT NOT NULL,
              tripId INT NOT NULL,
              FOREIGN KEY (stationNumber) REFERENCES station(stationNumber),
              FOREIGN KEY (nextStationNumber) REFERENCES station(stationNumber)
              )
              `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created Stop table");
  });


  //TRIP
  var sql = `CREATE TABLE IF NOT EXISTS Trip (
    tripId INT PRIMARY KEY NOT NULL,
    initialStationNumber INT NOT NULL,
    finalStationNumber INT NOT NULL,
    initialTime VARCHAR(255) NOT NULL,
    finalTime VARCHAR(255) NOT NULL,
    FOREIGN KEY (initialStationNumber) REFERENCES station(stationNumber),
    FOREIGN KEY (finalStationNumber) REFERENCES station(stationNumber)
    )
    `;
con.query(sql, function(err, result) {
if(err) throw err;
console.log("Created Trip table");
});



  //TICKET
  var sql = `CREATE TABLE IF NOT EXISTS Ticket (
              ticketId INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
              ticketUuid VARCHAR(255) UNIQUE NOT NULL ,
              userUuid VARCHAR(255) NOT NULL,
              initialStationNumber INT NOT NULL,
              finalStationNumber INT NOT NULL,
              price VARCHAR(255) NOT NULL,
              used BOOLEAN NOT NULL,
              date VARCHAR(255) NOT NULL,
              time VARCHAR(255) NOT NULL,
              FOREIGN KEY (userUuid) REFERENCES user(uuid),
              FOREIGN KEY (initialStationNumber) REFERENCES station(stationNumber),
              FOREIGN KEY (finalStationNumber) REFERENCES station(stationNumber)
              )
              `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created Ticket table");
  });


  //SEAT
  var sql = `CREATE TABLE IF NOT EXISTS Seat (
    ticketUuid VARCHAR(255) NOT NULL,
    stopId INT NOT NULL,
    date VARCHAR(255),
    FOREIGN KEY (ticketUuid) REFERENCES Ticket(ticketUuid),
    FOREIGN KEY (stopId) REFERENCES Stop(stopId)
    )
    `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created Seat table");
  });




  //TICKET STOPS - MANY TO MANY
  var sql = `CREATE TABLE IF NOT EXISTS Ticket_Stops (
    ticketId INT NOT NULL,
    stopId INT NOT NULL,
    FOREIGN KEY (ticketId) REFERENCES Ticket(ticketId),
    FOREIGN KEY (stopId) REFERENCES Stop(stopId)
    )
    `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created TicketStops table, a many-to-many relationship");
  });



  //REVIEW
  var sql = `CREATE TABLE IF NOT EXISTS review (
    ticketUuid VARCHAR(255) NOT NULL,
    rating VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    FOREIGN KEY (ticketUuid) REFERENCES Ticket(ticketUuid)
    )
    `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Created review table");
  });


  
  var InsUUID = uuidv4();
  var InsUUID2 = uuidv4();

    //INSERT INSPECTOR
    var sql = 'INSERT INTO inspector(uuid, name, password)'+
    'VALUES(\'' + InsUUID + '\', \'guilherme\', \'guilherme\'),' +
    '(\'' + InsUUID2 + '\',\'inspector\', \'password\')';
    con.query(sql, function(err, result) {
      if(err) throw err;
      console.log("Inserted values for trips");
    });



//INSERT STATIONS
  var sql = `INSERT INTO Station(stationNumber, name)
    VALUES(1, 'Aveiro'),
    (2, 'Estarreja'),
    (3, 'Espinho'),
    (4, 'Gaia'),
    (5, 'Porto')
  `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Inserted values for stations");
  });

  
  //INSERT TRIPS
  var sql = `INSERT INTO Trip(tripId, initialStationNumber, finalStationNumber, initialTime, finalTime)
  VALUES(1, 1, 5, '06:00', '8:00'),
  (2, 5, 1, '09:00', '11:00'),
  (3, 1, 5, '12:00', '14:00'),
  (4, 5, 1, '15:00', '17:00'),
  (5, 1, 5, '18:00', '20:00'),
  (6, 5, 1, '21:00', '23:00')
  `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Inserted values for trips");
  });


  //INSERT STOPS
  var seatsToNextStop = 2;
  var sql = `INSERT INTO Stop(stopId, stopTime, stationNumber, nextStationNumber, seatsToNextStop, tripId)
  VALUES(1, '06:00', 1, 2,  `+seatsToNextStop+`, 1),
  (2, '06:30', 2,  3, `+seatsToNextStop+`, 1),
  (3, '07:00', 3, 4, `+seatsToNextStop+`, 1),
  (4, '07:30', 4, 5, `+seatsToNextStop+`, 1),
  (5, '08:00', 5, NULL, `+seatsToNextStop+`, 1),
  (6, '09:00', 5, 4, `+seatsToNextStop+`, 2),
  (7, '09:30', 4, 3, `+seatsToNextStop+`, 2),
  (8, '10:00', 3, `+seatsToNextStop+`, 2, 2),
  (9, '10:30', 2, 1, `+seatsToNextStop+`, 2),
  (10, '11:00', 1, NULL, `+seatsToNextStop+`, 2),
  (11, '12:00', 1, `+seatsToNextStop+`, 2, 3),
  (12, '12:30', 2, 3, `+seatsToNextStop+`, 3),
  (13, '13:00', 3, 4, `+seatsToNextStop+`, 3),
  (14, '13:30', 4, 5, `+seatsToNextStop+`, 3),
  (15, '14:00', 5, NULL, `+seatsToNextStop+`, 3),
  (16, '15:00', 5, 4, `+seatsToNextStop+`, 4),
  (17, '15:30', 4, 3, `+seatsToNextStop+`, 4),
  (18, '16:00', 3, `+seatsToNextStop+`, 2, 4),
  (19, '16:30', 2, 1, `+seatsToNextStop+`, 4),
  (20, '17:00', 1, NULL, `+seatsToNextStop+`, 4),
  (21, '18:00', 1, `+seatsToNextStop+`, 2, 5),
  (22, '18:30', 2, 3, `+seatsToNextStop+`, 5),
  (23, '19:00', 3, 4, `+seatsToNextStop+`, 5),
  (24, '19:30', 4, 5, `+seatsToNextStop+`, 5),
  (25, '20:00', 5, NULL, `+seatsToNextStop+`, 5),
  (26, '21:00', 5, 4, `+seatsToNextStop+`, 6),
  (27, '21:30', 4, 3, `+seatsToNextStop+`, 6),
  (28, '22:00', 3, `+seatsToNextStop+`, 2, 6),
  (29, '22:30', 2, 1, `+seatsToNextStop+`, 6),
  (30, '23:00', 1, NULL, `+seatsToNextStop+`, 6)
  `;
  con.query(sql, function(err, result) {
    if(err) throw err;
    console.log("Inserted values for stops");
  });

});