const express = require('express'),
http = require('http'),
app = express(),
server = http.createServer(app),
io = require('socket.io').listen(server);

server.listen(3000,()=>{
    console.log('Node app is running on port 3000')
});

io.on('connection', (socket) => {

    console.log('User connected ' + socket.id)

});